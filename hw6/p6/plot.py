import sys
import json

import numpy
import matplotlib.pyplot as plt

data = json.loads(sys.stdin.read())
ones = numpy.array([[x['Intensity'], x['Symmetry']] for x in data if x['Digit'] == 1])
fives = numpy.array([[x['Intensity'], x['Symmetry']] for x in data if x['Digit'] == 5])

plt.scatter(ones[:,0], ones[:,1], marker='o', s=40, c='blue')
plt.scatter(fives[:,0], fives[:,1], marker='x', s=40, c='red')

plt.title("Features describing images of the digits '1', and '5'")
plt.xlabel("Average Intensity")
plt.ylabel("Symmetry")
plt.show()
