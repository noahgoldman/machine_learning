package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"strconv"
)

const (
	ImageDim = 16
)

type digitImage [ImageDim][ImageDim]float64

type Digit struct {
	digit  int
	values digitImage
}

func readNewDigit(in io.Reader) (*Digit, error) {
	scanner := bufio.NewScanner(in)
	scanner.Split(bufio.ScanWords)

	// Read the initial digit
	if !scanner.Scan() {
		if scanner.Err() != nil {
			return nil, fmt.Errorf("Failed to read inital digit: %v",
				scanner.Err())
		}
	}
	digitText := scanner.Text()
	digitFloat, err := strconv.ParseFloat(digitText, 64)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse %s as an int", digitText)
	}
	digit := int(digitFloat)

	// Read in the actual data for the digit
	var image digitImage
	for i := 0; i < ImageDim; i++ {
		for j := 0; j < ImageDim; j++ {
			if !scanner.Scan() {
				fmt.Printf("got %v with digit: %#v\n", digit, image)
				return nil, fmt.Errorf("Failed to read all input digits: %v",
					scanner.Err())
			}
			val, err := strconv.ParseFloat(scanner.Text(), 64)
			if err != nil {
				return nil, fmt.Errorf("Failed to parse pixel value: %s", scanner.Text())
			}

			image[i][j] = val
		}
	}

	return &Digit{digit: digit, values: image}, nil
}

// Compute average intensity of the image
// Defined as summing all pixels, then dividing by ImageDim^2
func (d *Digit) averageIntensity() float64 {
	var sum float64 = 0.0
	for i := range d.values {
		for j := range d.values[i] {
			sum += (d.values[i][j] + 1) / float64(2)
		}
	}

	return sum / math.Pow(ImageDim, 2)
}

// Compute symmetry of the image around the vertical center line
// sum(2-abs((i,j) - (16-i, j)))
func (d *Digit) horizontalSymmetry() float64 {
	var sum float64 = 0.0
	for i := range d.values {
		for j := 0; j < len(d.values[i])/2; j++ {
			sum += 2 - math.Abs(d.values[i][j]-d.values[i][len(d.values[i])-1-j])
		}
	}

	return sum / math.Pow(ImageDim, 2)
}
