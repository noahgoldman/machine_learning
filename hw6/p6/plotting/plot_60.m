% This M-file constructs the individual images for 60 digits
% and plots them to a file.

clear
format short g
load ZipDigits.train
digits=ZipDigits(:,1);
grayscale=ZipDigits(:,2:end);

[n,d]=size(grayscale);
w=floor(sqrt(d));

for i=1:1000
	[i, digits(i)]
    if digits(i) == 1 && i > 100
        curimage=reshape(grayscale(i,:),w,w);
        curimage=curimage';
        l=displayimage(curimage);
    end
end