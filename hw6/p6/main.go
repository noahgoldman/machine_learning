package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

var inputFile string

func init() {
	flag.StringVar(&inputFile, "i", "", "Input file with data")
}

func main() {
	flag.Parse()

	if inputFile == "" {
		log.Fatal("Must pass an input file with -i")
	}

	data, err := readDataFromFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	res, err := getIntensitySymmetryData(data)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(os.Stdout, "%s", res)
}

func readDataFromFile(inputFile string) ([]*Digit, error) {
	var err error
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Failed to read file %s", inputFile)
	}

	digits := []*Digit{}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		digit, err := readNewDigit(strings.NewReader(scanner.Text()))
		if err != nil {
			return nil, err
		}

		digits = append(digits, digit)
	}

	return digits, nil
}

func getIntensitySymmetryData(digits []*Digit) ([]byte, error) {
	type IntensitySymmetryResult struct {
		Digit     int
		Intensity float64
		Symmetry  float64
	}
	var res []*IntensitySymmetryResult

	for i := range digits {
		res = append(res, &IntensitySymmetryResult{
			Digit:     digits[i].digit,
			Intensity: digits[i].averageIntensity(),
			Symmetry:  digits[i].horizontalSymmetry(),
		})
	}

	return json.Marshal(res)
}
