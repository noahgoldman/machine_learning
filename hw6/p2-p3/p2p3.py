import itertools

import numpy as np
from numpy.lib.scimath import sqrt as csqrt
import matplotlib.pyplot as plt

LEARNING_RATE=1

def upper_bound(x, thk, rad, sep):
    upper = (csqrt((rad+thk)**2 - (x - (rad+thk))**2) + float(sep)/2).real
    lower = (csqrt(rad**2 - (x - (rad+thk))**2) + float(sep)/2).real

    if upper == lower:
        return None

    return [lower,  upper]

def lower_bound(x, thk, rad, sep):
    lower = (-1*csqrt((rad+thk)**2 - (x - 2*(rad+thk))**2) - float(sep)/2).real
    upper = (-1*csqrt(rad**2 - (x - 2*(rad+thk))**2) - float(sep)/2).real

    if upper == lower:
        return None

    return [lower,  upper]

bound_funcs = [upper_bound, lower_bound]

def generate_dataset(thk, rad, sep, n):
    outputs = []

    for _ in itertools.repeat(None, n):
        # pick a bound function
        bound_func_index = np.random.random_integers(0, len(bound_funcs)-1)
        bound_func = bound_funcs[bound_func_index]

        # pick x's until one is found in the appropriate range
        bound = None
        while bound is None:
            x = np.random.uniform(0, 3*(rad+thk))
            bound = bound_func(x, thk, rad, sep)

        val = np.random.uniform(bound[0], bound[1])

        target_val = bound_func_index if bound_func_index > 0 else -1
        outputs.append([x, val, target_val])

    return outputs

def plot_points(points):
    plt.scatter(points[:,0], points[:,1])
    plt.xlabel(r'$x_1$')
    plt.ylabel(r'$x_2$')

def plot_weights(w, rng):
    x = np.linspace(0, rng, num=10000)
    w = np.asfarray(w)
    y = -1*(w[1]/w[2])*x-(w[0]/w[2])
    plt.plot(x, y)

def pla(data):
    w = np.array([0, 0, 0])

    # append a column of ones to the data
    data = np.append(np.ones((len(data), 1)), data, axis=1)

    x = find_misclassified(data, w)
    j = 0
    while x is not None:
        for i in [1, 2]:
            w[i] = w[i] + LEARNING_RATE*x[3]*x[i]
        x = find_misclassified(data, w)
        j += 1

    return w, j

def find_misclassified(data, w):
    for x in data:
        if np.sign(np.dot(x[0:3], w)) != x[3]:
            return x
    return None

def p2a():
    points = np.array(generate_dataset(5, 10, 5, 2000))
    w, j = pla(points)
    plot_weights(w, 45)
    plot_points(points)

    plt.title("PLA output for dataset of size N=2000")
    plt.show()

def p2b():
    data = np.array(generate_dataset(5, 10, 5, 2000))
    with_ones = np.append(np.ones((len(data), 1)), data, axis=1)

    x = with_ones[:,:3]
    y = with_ones[:,3]

    w = np.dot(np.dot(np.linalg.inv(np.dot(np.transpose(x), x)), np.transpose(x)), y)
    print(w)

    plot_weights(w, 45)
    plot_points(data)

    plt.title("Linear regression output for dataset of size N=2000")
    plt.show()

def p3():
    seps = np.arange(0.2, 5, 0.2)
    iterations = []
    for sep in seps:
        print("doing sep: " + str(sep))

        iter_times = 20
        iter_sum = 0
        for _ in range(iter_times):
            points = np.array(generate_dataset(5, 10, sep, 2000))
            _, j = pla(points)
            iter_sum += j

        iterations.append(float(iter_sum)/float(iter_times))

    print(iterations)
    plt.plot(seps, iterations)

    plt.xlabel(r'Value of $sep$')
    plt.ylabel("Number of PLA iterations")
    plt.title(r'PLA iterations at different values of $sep$ averaged over 20 trials')
    plt.savefig("images/p3.png")

if __name__ == '__main__':
    p3()
