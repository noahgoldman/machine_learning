\documentclass{article}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}

\begin{document}

\title{Homework 6}
\author{Noah Goldman}
\maketitle

\section{Exercise 3.4}

\subsection{}

Using the given relationship $w_{lin}=X^{\dagger}y$ and the target function $y=w^{*T}x+\epsilon$, the value of $w_{lin}$ can be determined.

\begin{align*}
    w_{lin} &= X^{\dagger}y \\
    &= X^{\dagger}{+}(w^{*T}x+\epsilon)
\end{align*}

Using the relationship $\hat{y}=Xw_{lin}$, the value of $\hat{y}$ can be determined.

\begin{align*}
    \hat{y} &= Xw_{lin} \\
    &= X(X^{\dagger}{+}(w^{*T}X+\epsilon)) \\
    &= XX^{\dagger}w^{*T}X + XX^{\dagger}\epsilon
\end{align*}

The text defined the hat matrix $H=XX^{\dagger}$.  Additionally, we can say that $w^{*T}X=Xw^{*}$ by the dimensions of the two matrices, (the text indirectly states this on page 84).

\begin{align*}
    \hat{y} &= XX^{\dagger}Xw^{*}+H\epsilon
\end{align*}

By expanding $X^{\dagger}=(X^{T}X)^{-1}X^{T}$ and substituting, the expression $X(X^{T}X)^{-1}X^{T}X$ clearly includes $(X^{T}X)^{-1}X^{T}X$.  As the $X^{T}X$ is invertible by the definition of the problem and $A^{-1}A=I$ for any A, $(X^{T}X)^{-1}X^{T}X=I$. Therefore:

\begin{align*}
    \hat{y} &= Xw^{*}+H\epsilon
\end{align*}

\subsection{}

Using the relations $\hat{y} = Xw^{*}+H\epsilon$ (from 1.a) and $y=w^{*T}x+\epsilon$, we can determine $\hat{y} - y$. The relationship $w^{*T}X=Xw^{*}$ that was previously shown is also used.

\begin{align*}
    \hat{y} - y &= (Xw^{*} + H\epsilon) - (w^{*T}X + \epsilon) \\
    &= Xw^{*} + H\epsilon - w^{*T}X - \epsilon \\
    &= Xw^{*} + H\epsilon - Xw^{*} - \epsilon \\
    &= H\epsilon - \epsilon \\
    &= (H-1)\epsilon
\end{align*}

As shown above $\hat{y} - y = (H-I)\epsilon$.

\subsection{}

The book defines $E_{in}$ as the average of the squared errors between the produced targets (in this case $\hat{y}$ and the actual target $y$.  Expressing this in matrix form gives $E_{in}(w_{lin})=\frac{1}{N}(\hat{y}-y)^{T}(\hat{y}-y)$.

\begin{align*}
    E_{in}(w_{lin}) &= \frac{1}{N}(\hat{y}-y)^{T}(\hat{y}-y) \\
    &= \frac{1}{N}((H-I)\epsilon)^{T}((H-I)\epsilon) \\
    &= \frac{1}{N}\epsilon^{T}(H-I)^{T}(H-I)\epsilon
\end{align*}

The operations above were performed by substituting the result from part 1.b, and by distributing the transpose operator.  The equation can be further simplified by distributing the transpose in $(H-I)^{T}=H^{T}-I^{T}$.  Since $H$ is symmetric by the result of Exercise 3.3a and the identity matrix is symmetric, $(H-I)^{T}=H-I$.

\begin{align*}
    E_{in}(w_{lin}) &= \frac{1}{N}\epsilon^{T}(H-I)^{T}(H-I)\epsilon \\
    &= \frac{1}{N}\epsilon^{T}(H^{T} - I^{T})(H-I)\epsilon \\
    &= \frac{1}{N}\epsilon^{T}(H-I)(H-I)\epsilon
\end{align*}

Using the property from Exercise 3.3c that $(1-H)^{K}=1-H$, the equation can be further simplified.

\begin{align*}
    E_{in}(w_{lin}) &= \frac{1}{N}\epsilon^{T}(H-I)(H-I)\epsilon \\
    &= \frac{1}{N}\epsilon^{T}(-(I-H))^{2}\epsilon \\
    &= \frac{1}{N}\epsilon^{T}(I-H)\epsilon
\end{align*}

Thus $E_{in}(w_{lin})=\frac{1}{N}\epsilon^{T}(I-H)\epsilon$.

\subsection{}

First, we take the absolute value of $E_{in}(w_{lin})$ and expand terms.

\begin{align*}
    E_{in}(w_{lin}) &= \frac{1}{N}\epsilon^{T}(I-H)\epsilon \\
    \mathbb{E}[E_{in}(w_{lin})] &= \mathbb{E}[\frac{1}{N}\epsilon^{T}(I-H)\epsilon] \\
    &= \mathbb{E}[\frac{1}{N}\epsilon^{T}\epsilon - \frac{1}{N}\epsilon^{T}(I-H)\epsilon] \\
    &= \mathbb{E}[\frac{1}{N}\epsilon^{T}\epsilon] - \mathbb{E}[\frac{1}{N}\epsilon^{T}H\epsilon] \\
\end{align*}

Since $\epsilon^{T}\epsilon$ is effectively equal to $\sum\limits_{n=1}^{N} \epsilon_{n}^{2}$, by the definition of expected value $\frac{1}{N}\sum\limits_{n=1}^{N} \epsilon_{n}^{2}=\mathbb{E}[\epsilon^2]$.  Since the mean of $\epsilon$ is zero, we can say that the variance of $\epsilon$ is $\sigma=\mathbb{E}[(\epsilon-\mu_{\epsilon})^2]=\mathbb{E}[\epsilon^2]$.  Thus $\frac{1}{N}\epsilon^{T}\epsilon=\sigma^2$, and $\mathbb{E}[\frac{1}{N}\epsilon^T\epsilon]=\sigma^2$.

Considering $\mathbb{E}[\frac{1}{N}\epsilon^{T}H\epsilon]$, the product $\epsilon^{T}H\epsilon$ can be expanded to be $\sum\limits_{i=1}^N \sum\limits_{j=1}^N \epsilon_{i}\epsilon_{j}H_{i,j}$, by the dimensions of $\epsilon^T$, $H$, and $\epsilon$, and the semantics of matrix multiplication.  This summation can then be further separated into $\sum\limits_{i=1}^N \sum\limits_{j=1,j \neq i}^N \epsilon_{i}\epsilon_{j}H_{i,j} + \sum\limits_{i=1}^N \epsilon_{i}^2H_{i,i}$, where the second term in the summation is along the diagonal elements of $H$.  Taking the expected value of both of these terms is shown below:

\begin{align*}
    \mathbb{E}[\frac{1}{N}\epsilon^{T}H\epsilon] &= \mathbb{E}[\frac{1}{N}\sum\limits_{i=1}^N \sum\limits_{j=1,j \neq i}^N \epsilon_{i}\epsilon_{j}H_{i,j} + \sum\limits_{i=1}^N \epsilon_{i}^2H_{i,i}] \\
    &= \mathbb{E}[\frac{1}{N}(\sum\limits_{i=1}^N \sum\limits_{j=1,j \neq i}^N \epsilon_{i}\epsilon_{j}H_{i,j}] + \mathbb{E}[\sum\limits_{i=1}^N \epsilon_{i}^2H_{i,i})] \\
    &= \frac{1}{N}(\mathbb{E}[\sum\limits_{i=1}^N \sum\limits_{j=1,j \neq i}^N \epsilon_{i}\epsilon_{j}H_{i,j}] + \mathbb{E}[\sum\limits_{i=1}^N \epsilon_{i}^2H_{i,i}]) \\
\end{align*}

As $\mathbb{E}[\epsilon]$ is zero, the left summation disappears.  In the right summation, we know that $\mathbb{E}[\sum\limits_{i=1}^N \epsilon^2]=\sigma^2$.  Additionally, Exercise 3.3(d) states that the sum of diagonal elements in $H$, $trace(H)=d+1$.  Therefore, we can say that $\mathbb{E}[\sum\limits_{i=1}^N \epsilon_{i}^2H_{i,i}]=(d+1)\sigma^2$.

Since $\mathbb{E}[\frac{1}{N}\epsilon^{T}H\epsilon]=\frac{(d+1)\sigma^2}{N}$ and $\mathbb{E}[\frac{1}{N}\epsilon^{T}\epsilon]$:

\begin{align*}
    \mathbb{E}[E_{in}(w_{lin})] &= \mathbb{E}[\frac{1}{N}\epsilon^{T}\epsilon] - \mathbb{E}[\frac{1}{N}\epsilon^{T}H\epsilon] \\
    &= \sigma^2 - \frac{(d+1)\sigma^2}{N}
\end{align*}

Therefore $\mathbb{E}_{D}[E_{in}(w_{lin})] = \sigma^2(1 - \frac{d+1}{N})$.

\subsection{}

In the test dataset, the target function $y'=w^{*T}X+\epsilon'$ produces a new value of $\hat{y}-y'=(Xw^*+H\epsilon)-(w^{*T}X+\epsilon')=H\epsilon-\epsilon'$.  This allows for a new value of $E_{in}(w_{lin})$ to be computed.

\begin{align*}
    E_{test}(w_{lin}) &= \frac{1}{N}(\hat{y}-y')^T(\hat{y}-y') \\
    &= \frac{1}{N}(H\epsilon-\epsilon')^T(H\epsilon-\epsilon') \\
    &= \frac{1}{N}(\epsilon^{T}H - \epsilon'^T)(H\epsilon-\epsilon') \\
    &= \frac{1}{N}(\epsilon^{T}H\epsilon - \epsilon^{T}H\epsilon' - \epsilon'^{T}H\epsilon + \epsilon'^T\epsilon') \\
\end{align*}

Taking the expected value of the above relationship for $E_{test}(w_{lin})$ will cause the terms $\epsilon^{T}H\epsilon'$ and $\epsilon'^{T}H\epsilon$ to disappear, as $\epsilon$ and $\epsilon'$ are independent with means of zero. This reasoning is also used in part (d) of this question.

\begin{align*}
    \mathbb{E}[E_{test}(w_{lin})] &= \mathbb{E}[\frac{1}{N}(\epsilon^{T}H\epsilon + \epsilon'^{T}\epsilon')] 
\end{align*}

This quation for $E_{in}(w_{lin})$ is effectively the same as was seen in part (d), except for the reversed sign (changing a minus to a plus).  Assuming that $\epsilon'$ has the same variance as $\epsilon$, the exact same procedure can be performed as in part (d) to produce $\mathbb{E}_{D,\epsilon'}[E_{test}(w_{lin})] = \sigma^2(1+\frac{d+1}{N})$, where the change to $1+\frac{d+1}{N}$ from $1+\frac{d+1}{N}$ came from the change in sign seen in the equation for $E_{test}(w_{lin})$.

\section{Problem 3.1}

\subsection{}

The experiment was performed with 2000 samples distributed uniformly in the two regions.  The result in shown in the plot below.

\includegraphics[width=\linewidth]{images/p2a}

\subsection{}

The plot below shows a dataset with 2000 samples along with the line corresponding to weights determined through linear regression. Compared to the weights produced by the PLA, the linear regression method created weights showing a much more sloped line.  This is likely due to the fact that the weights produced through linear regression are meant to "fit" the data, so it seems reasonable that the $x_2$ component of the line would increase with increasing $x_1$.  As the number of -1 and +1 data points are not evenly distributed over the graph, the fit produced by linear regression should definitely have a slope to compensate for this inequality.

\includegraphics[width=\linewidth]{images/p2b}

\section{Problem 3.2}

The experiment given in the problem statement was performed for all values of $sep={0.2,0.4,...,5}$.  The data shown in both the table and plot below was determined by running 10 trials of the experiment for each value of sep, and averaging the results together.

\begin{table}[h]
    \centering
    \begin{tabular}{|l|l|} \hline
    \textit{\textbf{sep}} & \bf{Iterations} \\ \hline
    0.2 & 62.95 \\
    0.4 & 18.1     \\
    0.6   & 28.15     \\
    0.8   & 39.55     \\
    1.0   & 18.25     \\
    1.2   & 20.2     \\
    1.4   & 18.7     \\
    1.6   & 16.3     \\
    1.8   & 15.45     \\
    2.0   & 17.05     \\
    2.2   & 10.15     \\
    2.4   & 12.75     \\
    2.6   & 13.45     \\
    2.8   & 10.5     \\
    3.0   & 11.35     \\
    3.2   & 9.35     \\
    3.4   & 10.45     \\
    3.6   & 10.6     \\
    3.8   & 10.0     \\
    4.0   & 11.05     \\
    4.2   & 8.0     \\
    4.4   & 8.1     \\
    4.6   & 7.4     \\
    4.8   & 7.85     \\
    5.0   & 7.2 \\ \hline
    \end{tabular}
\end{table}

\includegraphics[width=\linewidth]{images/p3}

The data clearly shows that, in general, the number of iterations the PLA requires to converge decreases with increasing values of $sep$.  While variations in the data sets could easily cause this conclusion to not appear true all of the time, it has a theoretical justification.  In Problem 1.3, we prove that the number of iterations the PLA takes to converge $t$, is bounded by $t \leq \frac{R^2||w^{*}||^{2}}{\rho^{2}}$.  $R$ is defined as $R=max_{1 \leq n \leq N}||x_n||$, which clearly will increase as $sep$ increases, due to larger and larger $x_n$ values being present.  As per the previously described relationship, this will cause a corresponding increase in the number of iterations.  The parameter $\rho$ is given as $\rho=min_{1 \leq n leq N}y_n(w^{*T}x_n)$, which will increase as $sep$ increases, but much slower than $R$.  Since it is in the denominator of $t \leq \frac{R^2||w^{*}||^{2}}{\rho^{2}}$ it will slow down the growth of the number of iterations as $sep$ increases, but not enough to offset the increase due to $R$.


\section{Problem 3.8}

As given in the problem description, $E_{out}(h)=\mathbb{E}[(h(x)-y)^2]$.  By taking the derivative with respect to $h$ and using the property that $\frac{d}{dt}\mathbb{E}[x]=\mathbb{E}[\frac{d}{dt}x$ for any t and x, the equation can be simplified.

\begin{align*}
    E_{out}(h) &= \mathbb{E}[(h(x)-y)^2] \\
    \frac{d}{dh}E_{out}(h) &= \frac{d}{dh}\mathbb{E}[(h(x)-y)^2] \\
    0 &= \mathbb{E}[\frac{d}{dh}(h(x)-y)^2] \\
    &= \mathbb{E}[\frac{d}{dh}(h(x)^2-2h(x)y+y^2)] \\
    &= \mathbb{E}[\frac{d}{dh}(h(x)^2-2h(x)y+y^2)] \\
    &= \mathbb{E}[2h(x)-2y] \\
    &= 2\mathbb{E}[h(x)-y] \\
    &= \mathbb{E}[h(x)] - \mathbb{E}[y] \\
    \mathbb{E}[h(x)] &= \mathbb{E}[y]
\end{align*}

A property of conditional expectation is that for any random variables X and Y, $\mathbb{E}[\mathbb{E}[Y|X]]=\mathbb{E}[Y]$.  Therefore, we will let $h(x)=\mathbb{E}[y|x]$ and show that the above equation holds, which set the derivative of $E_{out}=0$.

\begin{align*}
    \mathbb{E}[h(x)] &= \mathbb{E}[y] \\
    \mathbb{E}[\mathbb{E}[y|x]] &= \mathbb{E}[y] \\
    \mathbb{E}[y] &= \mathbb{E}[y]
\end{align*}

As shown in the steps above, $\frac{dE_{out}(h)}{dh}=0$ when $h(x)=\mathbb{E}[y|x]$.  Therefore, the hypothesis that minimizes $E_{out}$ is $h^*(x)=\mathbb{E}[y|x]$.

Assuming that $h^*(x)$ is a deterministic target function, we can show that $\mathbb{E}[\epsilon(x)]=0$ in $y=h^*(x)+\epsilon(x)$.

\begin{align*}
    y &= h^*(x)+\epsilon(x) \\
    \mathbb{E}[y] &= \mathbb{E}[h^*(x)+\epsilon(x)] \\
    \mathbb{E}[y] &= \mathbb{E}[h^*(x)] + \mathbb{E}[\epsilon(x)] \\
    \mathbb{E}[y] &= \mathbb{E}[\mathbb{E}[y|x]] + \mathbb{E}[\epsilon(x)] \\
\end{align*}

After substituting $h^*(x)=\mathbb{E}[y|x]$, we can use the same property given before that for any random variables X and Y, $\mathbb{E}[\mathbb{E}[Y|X]]=\mathbb{E}[Y]$.

\begin{align*}
    \mathbb{E}[y] &= \mathbb{E}[\mathbb{E}[y|x]] + \mathbb{E}[\epsilon(x)] \\
    \mathbb{E}[y] &= \mathbb{E}[y] + \mathbb{E}[\epsilon(x)] \\
    0 &= \mathbb{E}[\epsilon(x)] \\
\end{align*}

As shown above, $\mathbb{E}[\epsilon(x)]=0$.

\stepcounter{section}
\section{Handwritten Digits Data - Obtaining Features}

\subsection{}

Two of the images were plotted using Matlab (a 1 and a 5) and are displayed below.

\begin{figure}[!tbph]
    \centering
    \begin{minipage}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{images/p6_1}
        \caption{Plot of a "1" digit}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{images/p6_5}
        \caption{Plot of a "5" digit}
    \end{minipage}
\end{figure}

\subsection{}

The two features chosen to measure properties useful in distinguishing between 1 and 5 were average intensity, and average symmetry.  Average intensity was simply defined as $\frac{1}{D^2}\sum\limits_{i=1}^{D}\sum\limits_{j=1}^{D}\frac{x_{ij}+1}{2}$ where D is the dimension of the image (D=16 in this problem's case), and $x$ is a two-dimensional matrix of size $DxD$ representing the greyscale values.  This equation simply sums all of the values, and divides by the number of pixels to get an average intensity.  The addition of $1$ and dividing by $2$ inside the summation is simply to map the greyscale values from the range [-1,1] onto [0,1] for easier analysis.

The second feature chosen was average symmetry of the image around the vertical center line.  This was defined as $\frac{1}{D^2}\sum\limits_{i=1}^{D}\sum\limits_{j=1}^{\frac{D}{2}}(2-|x_{ij}-x_{i,D-j}|)$, where again D is the dimension of the image, and $x$ represents the pixel values.  The summation effectively iterates through each row, and for each column to the left of the vertical center line, subtracts the symmetric pixel value.  The absolute value in $|x_{ij}-x_{i,D-j}|$ guarantees that the subtraction will be positive, and the addition of $2$ in $2-|x_{ij}-x_{i,D-j}|$ transforms the outputted number to be higher in the case of more symmetry.  Finally, the product of the sum with $\frac{1}{D^2}$ computes the average symmetry.

\subsection{}

The plot of features is shown below, where 5's are denoted with a red 'x', and 1's are marked with a blue 'o'.

\includegraphics[width=\textwidth]{images/p6c}

\end{document}
