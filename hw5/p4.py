import numpy
import matplotlib.pyplot as plt

# Return n lists of 2 values sampled from [-1, 1]
def generate_dataset(n):
    return numpy.random.uniform(-1, 1, (n, 2))

def compute_a_b(n):
    data = generate_dataset(n)

    res = []
    for dataset in data:
        a_b = [dataset[0] + dataset[1], dataset[1]*dataset[0]]
        res.append(a_b)

    return numpy.array(res)

def get_a_b_average(n):
    data = compute_a_b(n)

    a_sum = 0
    b_sum = 0
    for row in data:
        a_sum += row[0]
        b_sum += row[1]

    return a_sum/len(data), b_sum/len(data)

def plot_average(n):
    # get data for x^2
    x = numpy.linspace(-1, 1, 1000)
    x_squared = x**2

    a_b = get_a_b_average(n)
    print(a_b)
    g_bar = a_b[0]*x + a_b[1]

    x2_handle = plt.plot(x, x_squared, label='f(X)')
    g_bar_handle = plt.plot(x, g_bar, label='g_bar(x)')

    plt.ylim(-0.1, 1)
    plt.legend(loc='best')

    plt.title("Target function and average hypothesis from [-1,1]")
    plt.show()


if __name__ == '__main__':
    plot_average(100000)
