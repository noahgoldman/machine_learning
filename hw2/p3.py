import numpy as np
from scipy.special import comb
import matplotlib.pyplot as plt
import math

def rand_flips(n):
   return (float(np.random.binomial(n, 0.5)) / n)

def experiment2():
    return [rand_flips(6), rand_flips(6)]

# return a tuple of (v1, vrand, vmin)
def experiment(num_coins):
    vals = [rand_flips(10) for _ in range(num_coins)]
    return [vals[0], vals[np.random.randint(len(vals))], np.amin(vals)]

def multiple_experiments(n, num_coins):
    lst = []
    for _ in range(n):
        lst.append(experiment(num_coins))

    return np.asarray(lst)

def multiple_experiments2(n, num_coins):
    lst = []
    for _ in range(n):
        lst.append(experiment2())

    return np.asarray(lst)

def part_a():
    res = experiment(1000)
    return sum(res) / len(res)

def part_b():
    data = multiple_experiments(10000, 1000)

    names = [r'$v_1$', r'$v_{rand}$', r'$v_{min}$']

    for i in range(data.shape[1]):
        plt.hist(data[:,i], bins=np.linspace(0, 1, num=11))
        plt.title(names[i] + ' from 10000 samples')
        plt.xlabel(r'$\mu$ (Population Density)')
        plt.ylabel('Frequency')
        plt.show()

def get_proportion_with_error(arr, mean, error):
    total = 0
    for x in arr:
        if abs(x - mean) > error:
           total += 1

    return total / len(arr)

def part_c():
    num_experiments = 10000
    data = multiple_experiments(num_experiments, 1000)

    names = [r'$v_1$', r'$v_{rand}$', r'$v_{min}$']

    for i in range(data.shape[1]):
        row_data = data[:,i]
        #avg = np.average(row_data)
        avg = 0.5
        sample_pts = np.linspace(0, 1, num=1000)
        actuals = [get_proportion_with_error(row_data, avg, x) for x in sample_pts]
        hoeffding = [2*math.exp(-2*(x**2)*10) for x in sample_pts]

        plt.plot(sample_pts, actuals, label="Simulated Data")
        plt.plot(sample_pts, hoeffding, label="Hoeffding Bound")
        plt.legend(loc='best')

        plt.title('Probability of a certain amount of error from the mean for ' + names[i])
        plt.xlabel(r'$\epsilon$ (Distance from the mean)')
        plt.ylabel("Probability")
        plt.show()

def get_proportion_max_with_error(data, mean, error):
    total = 0
    for x in data:
        if abs(x[0] - mean) > error or abs(x[1] - mean) > error:
            total += 1

    return float(total) / len(data)

def p7_b():
    num_experiments = 1000
    data = multiple_experiments2(num_experiments, 2)

    avg = 0.5
    sample_pts = np.linspace(0, 1, num=1000)
    actuals = [get_proportion_max_with_error(data, avg, x) for x in sample_pts]
    hoeffding = [4*math.exp(-2*(x**2)*6) for x in sample_pts]

    plt.plot(sample_pts, actuals, label="Two Coins")
    plt.plot(sample_pts, hoeffding, label="Hoeffding Bound")
    plt.legend(loc='best')

    plt.title('Probability of a certain amount of error from the mean for two coins flipped 6 times')
    plt.xlabel(r'$\epsilon$ (Distance from the mean)')
    plt.ylabel("Probability")
    plt.show()
