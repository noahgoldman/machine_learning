package main

import (
	"./nn"
	"math"
)

var points []*nn.Point = []*nn.Point{
	&nn.Point{[]float64{1, 0}, -1},
	&nn.Point{[]float64{0, 1}, -1},
	&nn.Point{[]float64{0, -1}, -1},
	&nn.Point{[]float64{-1, 0}, -1},
	&nn.Point{[]float64{0, 2}, 1},
	&nn.Point{[]float64{0, -2}, 1},
	&nn.Point{[]float64{-2, 0}, 1},
}

func main() {
	part_b()
}

func part_a() {
	nn.PlotKNN(points, points, 3)
}

func part_b() {
	new_pts := make(nn.Points, len(points))

	for i := range points {
		norm := math.Sqrt(math.Pow(points[i].Coords[0], 2) + math.Pow(points[i].Coords[1], 2))
		new_pts[i] = &nn.Point{
			[]float64{norm, math.Atan(points[i].Coords[0] / points[i].Coords[1])},
			points[i].Class,
		}
	}

	nn.PlotKNN(new_pts, points, 3)
}
