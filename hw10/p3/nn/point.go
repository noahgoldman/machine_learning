package nn

import (
	"fmt"
	"math"
	"sort"
)

type Point struct {
	Coords []float64

	Class int
}

type Points []*Point

func (pts Points) Len() int { return len(pts) }
func (pts Points) XY(i int) (float64, float64) {
	pt := pts[i]
	if len(pt.Coords) != 2 {
		panic(fmt.Errorf("The Points type shouldn't be used unless there are only 2 dims"))
	}

	return pt.Coords[0], pt.Coords[1]
}

func (p *Point) runKNN(points []*Point, k int) int {
	if len(points) < k {
		panic(fmt.Errorf("The input points to runKNN (length %d) should be at least k=%d",
			len(points), k))
	}
	if k%2 == 0 {
		panic(fmt.Errorf("K must be odd, (given k=%d)", k))
	}

	// Make a copy of the original array
	pts := append([]*Point(nil), points...)

	// Sort by distance
	sort.Sort(pointsByDistance{points: pts, center: p})

	classes := make(map[int]int)
	for i := range pts[:k] {
		classes[pts[i].Class] += 1
	}

	// Pick the max class and return it
	maxClass := 0
	maxCount := 0

	for k, v := range classes {
		if v > maxCount {
			maxCount = v
			maxClass = k
		}
	}

	return maxClass
}

// Compute the euclidean distance between two points
func (p *Point) distance(pt *Point) float64 {
	if len(p.Coords) != len(pt.Coords) {
		panic(fmt.Errorf("The two points %v and %v aren't in the same number of dimensions",
			p, pt))
	}

	sum := 0.0
	for i := range p.Coords {
		sum += math.Pow(p.Coords[i]-pt.Coords[i], 2)
	}

	return math.Sqrt(sum)
}

// Types for sorting by distance
type pointsByDistance struct {
	points []*Point
	center *Point
}

func (b pointsByDistance) Len() int      { return len(b.points) }
func (b pointsByDistance) Swap(i, j int) { b.points[i], b.points[j] = b.points[j], b.points[i] }
func (b pointsByDistance) Less(i, j int) bool {
	return b.points[i].distance(b.center) < b.points[j].distance(b.center)
}
