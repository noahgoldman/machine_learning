package nn

import (
	"fmt"
	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg/draw"
	"image/color"
	"math"
)

const (
	GRID_DIM = 500

	IMG_WIDTH  = 600
	IMG_HEIGHT = 400
)

func PlotKNN(pts Points, plot_pts Points, k int) {
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = fmt.Sprintf("Decision regions for the %d-NN rule", k)
	p.X.Label.Text = "X1"
	p.Y.Label.Text = "X2"

	p.Add(plotter.NewGrid())

	plusPoints, minusPoints := makeNNPoints(pts, plot_pts, k)

	plus_scatter, err := plotter.NewScatter(plusPoints)
	if err != nil {
		panic(err)
	}
	plus_scatter.GlyphStyle.Color = color.RGBA{R: 102, G: 178, B: 255, A: 255}

	minus_scatter, err := plotter.NewScatter(minusPoints)
	if err != nil {
		panic(err)
	}
	minus_scatter.GlyphStyle.Color = color.RGBA{R: 255, G: 102, B: 102, A: 255}

	actual_pts, err := plotter.NewScatter(plot_pts)
	if err != nil {
		panic(err)
	}
	actual_pts.GlyphStyle.Color = color.RGBA{G: 255, A: 255}
	actual_pts.GlyphStyle.Shape = draw.CircleGlyph{}

	p.Add(plus_scatter, minus_scatter, actual_pts)

	if err := p.Save(IMG_WIDTH, IMG_HEIGHT, "out.png"); err != nil {
		panic(err)
	}
}

func makeNNPoints(pts Points, plot_pts Points, k int) (Points, Points) {
	min, max := get2DLimits(plot_pts)

	x_span := max.Coords[0] - min.Coords[0]
	min_x := min.Coords[0] - 0.4*x_span
	max_x := max.Coords[0] + 0.8*x_span
	step_x := x_span / float64(GRID_DIM)

	y_span := max.Coords[1] - min.Coords[1]
	min_y := min.Coords[1] - 0.1*y_span
	max_y := max.Coords[1] + 0.1*y_span
	step_y := y_span / float64(GRID_DIM)

	plusPoints := make(Points, 0, GRID_DIM*GRID_DIM)
	minusPoints := make(Points, 0, GRID_DIM*GRID_DIM)

	for i := min_y; i < max_y; i += step_y {
		for j := min_x; j < max_x; j += step_x {
			pt := &Point{Coords: []float64{j, i}}

			//if zTransform(pt).runKNN(pts, k) > 0 {
			if pt.runKNN(pts, k) > 0 {
				plusPoints = append(plusPoints, pt)
			} else {
				minusPoints = append(minusPoints, pt)
			}
		}
	}

	return plusPoints, minusPoints
}

func zTransform(p *Point) *Point {
	x := math.Sqrt(math.Pow(p.Coords[0], 2) + math.Pow(p.Coords[1], 2))
	y := math.Atan(p.Coords[0] / p.Coords[1])
	return &Point{Coords: []float64{x, y}, Class: p.Class}
}
