\documentclass{article}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\usepackage{amsmath}
\usepackage{float}
\usepackage{graphicx}
\usepackage{amsfonts}

\graphicspath{{images/}}

\begin{document}

\title{Homework 10}
\author{Noah Goldman}
\maketitle

\section{Exercise 6.1}

\subsection{}

In general, any vectors in the same direction should have a cosine similarity of exactly $1$.  Therefore, any two vectors in the same direction but with significantly different magnitudes should have high cosine similarity with a very low Euclidean distance similarity.  One such example in two-dimensions are the vectors $x=\begin{bmatrix} 1 \\ 0 \end{bmatrix}$ and $x\prime=\begin{bmatrix} \infty \\ 0 \end{bmatrix}$.  The cosine similarity is $\text{CosSim}(x,x\prime)=\frac{x \cdot x\prime}{||x||x\prime||}=1$ and the euclidean distance is $d(x,x\prime)=||x-x\prime||=\infty$, showing high cosine similarity and low eucliean similarity.

The opposite situation can be used in the case of low consine similarity with high euclidean similarity.  Consider for example, the vectors $x=\begin{bmatrix} 1 \\ 0 \end{bmatrix}$ and $x=\begin{bmatrix} -1 \\ 0 \end{bmatrix}$.  As they are in opposit directions with equal magnitudes, the cosine similarity will be $\text{CosSim}(x,x\prime)=\frac{x \cdot x\prime}{||x||x\prime||}=-1$ while the euclidean distance becomes zero.  Thus, these two vectors show low cosine similarity with high euclidean similarity.

\subsection{}

If the origin of the coordinate system changes, euclidean similarity will not be effected as it takes the difference between two vectors or points.  A change in the coordinate system will effect both points or vectors equally, causing the euclidean distance to stay constant.  On the other hand, cosine similarity will be effected by the nature of the formula $\text{CosSim}(x,x\prime)=\frac{x \cdot x\prime}{||x||x\prime||}=1$.  Moving the coordinate system will cause the cosine similarity to certainly change, as the reference from which the angles between two points or vectors will have changed.

The fact that cosine similarity is affected by a change in the coordinate system certainly has an implication on the choice of features.  Ideally, features should be chosen such if they depend on a similar measure to cosine similarity, the origin of the coordinate system is not moved.  Otherwise, similarities between different data points would undesirably change.

\section{Exercise 6.2}

The probability of error on a test point can be re-written in terms of the two cases that can cause the event to occur.

\begin{align*}
  e(f(x)) &= \mathbb{P}[f(x) \ne y] \\
  &= \mathbb{P}[f(x)=1,y=-1] + \mathbb{P}[f(x)=-1,y=1]
\end{align*}

These cases can be considered separately based on the value of $\pi(x)$.  If $\pi(x) \geq \frac{1}{2}$, then the probability of an error should be $\mathbb{P}[f(x)=1,y=-1]=1-\pi(x)$.  This can be clearly seen from the definition $\pi(x)=\mathbb{P}[y=+1|x]$.  Additionally, when $\pi(x) \le \frac{1}{2}$, the probability of an error should be $\mathbb{P}[f(x)=-1,y=1]=\pi(x)$, again by the definition of $\pi(x)$.  Therefore we can redefine the error on a test point $x$.

\[e(f(x)) = \begin{cases}
    1 - \pi(x) & \pi(x) \geq \frac{1}{2} \\
    \pi(x) & \text{otherwise}
\end{cases}
\]

Given that the domain of $\pi(x)$ is $[0,1]$, we can say that $\pi(x) < 1 - \pi(x)$ for $\pi(x) < \frac{1}{2}$, and that $1 - \pi(x) \leq \pi(x)$ for $\pi(x) \geq \frac{1}{2}$.  These facts allow $e(f(x))$ to again be redefined as:

\[e(f(x)) = \mathbb{P}[f(x) \ne y] = \text{min}\{\pi(x), 1-\pi(x)\} \]

This error shown above $e(f(x))$ is the minimum probability of error for any hypothesis $h$.  Any hypothesis $h$ will fail to predict an $x$ with $y=1$ with at the least probability $1-\pi(x)$.  Additionally, no hypothesis will fail to predict an $x$ with $y=-1$ with anything less than probability $\pi(x)$.  Since the probabilities of an error for any hypothesis in both cases has been shown to be bounded, we can say that for any hypothesis $h$, $e(f(x)) \leq e(h(x))$.

\section{Problem 6.1}

\subsection{}

The decision regions were generated using the nearest neighbor algorithm for both the 1-NN rule (Figure \ref{1nn}) and 3-NN rule (Figure \ref{3nn}).

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p3a_1nn}
  \caption{Plot of decision regions for the 1-NN rule}
  \label{1nn}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p3a_3nn}
  \caption{Plot of decision regions for the 3-NN rule}
  \label{3nn}
\end{figure}

\subsection{}

The non-linear transform given in the problem statement was used to transform the data before running the nearest neighbor algorithm.  The classification regions are shown below in the x-space for the 1-NN rule (Figure \ref{1nnz}) and the 3-NN rule (Figure \ref{3nnz}) for the data from the z-space.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p3b_1nn}
  \caption{Plot of decision regions for the 1-NN rule using the given transform}
  \label{1nnz}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p3b_3nn}
  \caption{Plot of decision regions for the 3-NN rule using the given transform}
  \label{3nnz}
\end{figure}

\section{Problem 6.4}

The decision regions for the double semi-circle problem from Problem 3.1 are shown in Figures \ref{p4_1nn} and \ref{p4_3nn}.  These were computed using the nearest neighbor algorithm.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p4_1nn}
  \caption{Plot of decision regions for the 1-NN rule for the double semi-circle}
  \label{p4_1nn}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p4_3nn}
  \caption{Plot of decision regions for the 3-NN rule for the double semi-circle}
  \label{p4_3nn}
\end{figure}

\section{Problem 6.16}

\subsection{}

As described in the problem, 10,000 data points were generated in the unit square, and a 10-partition was generated for the data using the greedy method from the book.  A plot containing all of the data and the 10 partitions (indicated by the red circles) is given in Figure \ref{10part}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p5_ai}
  \caption{Plot of data points with 10 partitions for Problem 5.a(i)}
  \label{10part}
\end{figure}

This data and the generated clusters were then used to compare the running times of obtaining the nearest neighbor for each of 10,000 test data points using the brute force algorithm as well as branch and bound.  Computing the nearest neighbors for all 10,000 points took 10.8038 seconds for the brute force method, while branch and bound took only 1.7849 seconds.  This seems completely reasonable, as the branch and bound should be significantly faster then the brute force approach (6x speed improvement) due to the nature of its clustering.

\subsection{}

10,000 data points were again generated, this time using 10 identical gaussian centers with equal covariances of $\sigma I$ where $\sigma=0.1$.  The 10 partitions were again generated using the same algorithm as before, and they are shown along with the data in Figure \ref{10part-gaussian}.  These regions show a huge amount of overlap, which likely indicates that the generated data is too strongly clustered around the center.  This makes a good amount of sense, as the covariance of $\sigma=0.1$ seems to be a very high value for this application.  Lower values of $\sigma$ would likely result in much more defined gaussian centers, causing the clustering algorithm to produce better and clearer results.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p5_bi}
  \caption{Plot of data points with 10 partitions for gaussian centers from Problem 5.b(i)}
  \label{10part-gaussian}
\end{figure}

Again, the data and generated clusters were used to compare the running times of obtaining the nearest neighbor using a brute-force approach as well as branch and bound.  When running both on the data made using gaussian centers, brute force took 10.8869 seconds while branch and bound took 5.4555 seconds.  This huge increase in the running time of branch and bound is almost certainly due to the fact that the clusters have much larger radii then were seen in the uniform distribution case.  This will cause the branch and bound algorithm to run brute-force searches on a larger portion of the entire data set on average, increasing the overall running time.

\subsection{}

As previously described, branch and bound seems to provide large increases in speed when the clusters are pre-computed.  The 6 times speed increase seen with the uniform distribution shows that branch and bound can be very effective, even when the clusters aren't very well spread out (as seen from Figure \ref{10part}).  On the other hand, it is clear that the running time of the branch and bound algorithm heavily depends on the quality of the clustering process.  When the clusters aren't very far apart and have large radii (as seen in Figure \ref{10part-gaussian}) the algorithm takes a significantly longer time.  Therefore, it is clear that branch and bound should ideally be used in situations where the clustering algorithm is able to create clusters from the data that have fairly well-spaced centers, with small radii.

\subsection{}

Yes, the decision weather or not to use branch and bound definitely depends on the number of test points for which the nearest neighbor must be calculated.  As most of the speed increases that result from branch and bound come from the fact that the regions are pre-computed for the data, we can effectively consider the clustering process as having a fixed cost as the number of data points change.  Thus, for small data sets, the clustering process will take a significant amount of time while computations needed to find the nearest neighbors take a relatively short time.  As the data set grows large, the time savings dervied from clustering the data beforehand become more and more significant.  Thus, branch and bound should become more effective as the number of test points increases.

\end{document}
