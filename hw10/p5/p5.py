import random
import time

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge

import nn

COVARIANCE = 0.1

def generate_points(n):
    return np.random.rand(n, 2)

def generate_points_gaussian(n, g):
    centers = np.random.rand(g, 2)
    cov = COVARIANCE*np.identity(2)

    points = np.empty([n, 2])
    for i in range(n):
        pt = generate_gaussian_point(centers, cov)
        points[i,:] = pt

    return points

def generate_gaussian_point(centers, cov):
        center = random.choice(centers)
        return np.random.multivariate_normal(center, cov)

def make_clusters(pts, m):
    centers = np.empty([m, 3])
    centers[0,:] = np.append(random.choice(pts), 0)

    # Compute the farthest point from each center to populate the list
    for i in range(m-1):
        farthest = nn.find_farthest_from_set(np.array(centers), pts)
        centers[i+1,:] = np.append(farthest, i+1)

    clusters = []
    for i in range(5):
        clusters = compute_veronoi_from_centers(centers, pts)
        centers = np.array([cluster[0] for cluster in clusters])

    return clusters

def get_centers_from_clusters(clusters):
    return np.array([cluster[0] for cluster in clusters])

def compute_veronoi_from_centers(centers, pts):
    # Compute m different sets containing all points in the veronoi region
    regions = [[] for _ in range(len(centers))]
    for pt in pts:
        regions[int(nn.knn(pt, centers, 1))].append(pt)
    regions = [np.array(region) for region in regions]

    clusters = []
    for i in range(len(regions)):
        center = np.append(np.sum(regions[i], axis=0)/len(regions[i]), i)
        radius = nn.distance(nn.find_farthest(center[:2], regions[i]), center)
        clusters.append((center, radius))

    return clusters

def get_pts_in_cluster(cluster, pts):
    distances = np.sqrt(((pts[:,:2] - cluster[0][:2])**2).sum(axis=1))
    indices = np.where(distances < cluster[1])
    return np.take(pts, indices, axis=0)[0]

def make_regions(pts, m):
    clusters = make_clusters(pts, m)
    centers = get_centers_from_clusters(clusters)
    return clusters, [get_pts_in_cluster(cluster, pts) for cluster in clusters]

def bb_find_nearest(x, clusters, regions):
    centers = get_centers_from_clusters(clusters)

    # Get the closest center
    closest_indices = nn.find_nearest_indices(x, centers, 2)
    closest = clusters[closest_indices[0]]
    closest2 = clusters[closest_indices[1]]

    # Get all points in the region and find the nearest to x
    region = regions[closest_indices[0]]
    closest_pt = nn.find_nearest(x, region, 1)[0]

    return closest_pt

def plot_clusters(clusters):
    ax = plt.axes()
    for cluster in clusters:
        ring = Wedge((cluster[0][0], cluster[0][1]), cluster[1], 0, 360,
                color='r', width=0.003)
        ax.add_patch(ring)

def plot_points(pts):
    plt.scatter(pts[:,0], pts[:,1])

def p5i(pts):
    clusters = make_clusters(pts, 10)
    plot_clusters(clusters)
    plot_points(pts)

    plt.xlim(0, 1)
    plt.ylim(0, 1)

    plt.title("10-partition constructed for 10,000 points generated with 10 gaussian centers")
    print('hi')
    plt.show()
    print('ho')

def p5ii(pts):
    test_pts = generate_points(10000)
    clusters, regions = make_regions(pts, 10)

    start = time.time()
    i = 0
    for pt in test_pts:
        nn.find_nearest(pt, pts, 1)
        if i % 1000 is 0:
            print(i)
        i += 1
    print("NN Took {}".format(time.time() - start))

    start = time.time()
    i = 0
    for pt in test_pts:
        bb_find_nearest(pt, clusters, regions)
        if i % 1000 is 0:
            print(i)
        i += 1
    print("BB Took {}".format(time.time() - start))

def p5ai(n):
    pts = generate_points(n)
    p5i(pts)

def p5aii(n):
    pts = generate_points(n)
    p5ii(pts)

def p5bi(n):
    pts = generate_points_gaussian(n, 10)
    p5i(pts)

def p5bii(n):
    pts = generate_points_gaussian(n, 10)
    p5ii(pts)

if __name__ == '__main__':
    p5bii(10000)
