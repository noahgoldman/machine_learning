import math

import numpy as np

def knn(x, points, k):
    nearest = find_nearest(x, points, k)

    vals, counts = np.unique(nearest[:,2], return_counts=True)
    return vals[np.argmax(counts)]

def find_nearest_indices(x, points, k):
    distances = np.sqrt(((points[:,:2] - x)**2).sum(axis=1))
    return np.argsort(distances)[:k]

def find_nearest(x, points, k):
    indices = find_nearest_indices(x, points, k)
    return np.take(points, indices, axis=0)

def find_farthest(x, points):
    distances = np.sqrt(((points[:,:2] - x)**2).sum(axis=1))
    return points[np.argsort(distances)[-1]]

def find_farthest_from_set(x_set, points):
    distances = []

    for pt in points:
        nearest = find_nearest(pt, x_set, 1)[0]
        distances.append(distance(nearest, pt))

    indices = np.argsort(distances)
    return points[indices[-1]]

def distance(x1, x2):
    return np.sqrt(((x1[:2] - x2[:2])**2).sum())
