import itertools

import numpy as np
from numpy.lib.scimath import sqrt as csqrt
import matplotlib.pyplot as plt

import nn

LEARNING_RATE=1
GRID_DIM=400

def upper_bound(x, thk, rad, sep):
    upper = (csqrt((rad+thk)**2 - (x - (rad+thk))**2) + float(sep)/2).real
    lower = (csqrt(rad**2 - (x - (rad+thk))**2) + float(sep)/2).real

    if upper == lower:
        return None

    return [lower,  upper]

def lower_bound(x, thk, rad, sep):
    lower = (-1*csqrt((rad+thk)**2 - (x - 2*(rad+thk))**2) - float(sep)/2).real
    upper = (-1*csqrt(rad**2 - (x - 2*(rad+thk))**2) - float(sep)/2).real

    if upper == lower:
        return None

    return [lower,  upper]

bound_funcs = [upper_bound, lower_bound]

def generate_dataset(thk, rad, sep, n):
    outputs = []

    for _ in itertools.repeat(None, n):
        # pick a bound function
        bound_func_index = np.random.random_integers(0, len(bound_funcs)-1)
        bound_func = bound_funcs[bound_func_index]

        # pick x's until one is found in the appropriate range
        bound = None
        while bound is None:
            x = np.random.uniform(0, 3*(rad+thk))
            bound = bound_func(x, thk, rad, sep)

        val = np.random.uniform(bound[0], bound[1])

        target_val = bound_func_index if bound_func_index > 0 else -1
        outputs.append([x, val, target_val])

    return outputs

def plot_points(points):
    plus = []
    minus = []

    for point in points:
        lst = plus if point[1] < 0 else minus
        lst.append(point)

    plus = np.array(plus)
    minus = np.array(minus)

    plt.scatter(plus[:,0], plus[:,1], edgecolors='b', facecolors='none')
    plt.scatter(minus[:,0], minus[:,1], marker='x', c='r')

    plt.xlabel(r'$x_1$')
    plt.ylabel(r'$x_2$')

def generate_nn_data(points, k, rad, thk, sep):
    ylims = (-1*((rad+thk)+(sep/2))-1, (rad+thk)+(sep/2) + 1)
    xlims = (-4, 3*(rad+thk)+4)

    plus = []
    minus = []

    for y in np.linspace(ylims[0], ylims[1], num=GRID_DIM):
        for x in np.linspace(xlims[0], xlims[1], num=GRID_DIM):
            pt = [x, y]
            lst = plus if nn.knn(pt, points, k) > 0 else minus
            lst.append(pt)

    plt.xlim(xlims)
    plt.ylim(ylims)

    return np.array(plus), np.array(minus)

def plot_nn_points(plus, minus):
    plt.scatter(plus[:,0], plus[:,1], s=150, facecolors='#66ccff', edgecolors='none')
    plt.scatter(minus[:,0], minus[:,1], s=150, facecolors='#ff9999', edgecolors='none')

def p4(k):
    rad = 10
    thk = 5
    sep = 5

    points = np.array(generate_dataset(thk, rad, sep, 2000))
    plus, minus = generate_nn_data(points, k, rad, thk, sep)

    plot_nn_points(plus, minus)
    plot_points(points)

    plt.title("Decision regions for the {}-NN rule".format(k))
    plt.show()

if __name__ == '__main__':
    p4(1)
    p4(3)
