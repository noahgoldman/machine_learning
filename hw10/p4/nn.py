import math

import numpy as np

def knn(x, points, k):
    nearest = find_nearest(x, points, k)

    vals, counts = np.unique(nearest[:,2], return_counts=True)
    return vals[np.argmax(counts)]

def find_nearest(x, points, k):
    distances = np.sqrt(((points[:,:2] - x)**2).sum(axis=1))
    indices = np.argsort(distances)[:k]
    return np.take(points, indices, axis=0)
