import sys

import numpy as np
import matplotlib.pyplot as plt

THRESHOLD = 0.5
LEARNING_RATE = 0.2

# Generate the set
def gen_data(n):
    data = np.random.rand(n, 3)

    # set the first column to 1
    for row in data:
        row[0] = 1

    return data

def gen_target_func():
    b = np.random.rand()
    m = np.random.rand()*2-1

    w2 = -1*THRESHOLD/b
    w = np.array([THRESHOLD, -w2*m, w2])
    return make_func_from_weights(w), w

def make_func_from_weights(w):
    return lambda x: -w[1]/w[2] * x - w[0]/w[2]

# Plotting functions
def plot(data, target):
    plot_data(data)
    plot_target(target)
    plt.show()

def plot_data(data, target_weights):
    for row in data:
        if y(row, target_weights) > 0:
            plt.scatter(row[1], row[2], s=50, marker='x')
        else:
            plt.scatter(row[1], row[2], s=50, marker='.')

def plot_func(func, name):
    x = np.linspace(0, 1, num=1000)
    y = func(x)
    plt.plot(x, y, label=name)

def pla(data, target_weights):
    w = np.array([0.5, 0, 0])

    x = find_misclassified(data, w, target_weights)
    j = 0
    while x is not None:
        for i in [1, 2]:
            w[i] = w[i] + LEARNING_RATE*y(x, target_weights)*x[i]
        x = find_misclassified(data, w, target_weights)
        j += 1

    return w, j

def find_misclassified(data, w, target_weights):
    for x in data:
        if y(x, w) != y(x, target_weights):
            return x
    return None

def y(x, w):
    return np.sign(np.dot(x, w))

def final_plot(data, size, steps, target_weights, final_weights, name):
    plot_data(data, target_weights)
    plot_func(make_func_from_weights(target_weights), 'Target')
    plot_func(make_func_from_weights(final_weights), 'Computed')

    plt.title("Ran PLA for {0} iterations on a data set of size {1}".format(steps, size))
    plt.xlabel("X1 (first dimension)")
    plt.ylabel("X2 (second dimension)")
    plt.savefig(name + '.png')
    plt.clf()

def main():
    data = gen_data(20)
    f, target_weights = gen_target_func()
    final, steps = pla(data, target_weights)

    plot_data(data, target_weights)
    plot_func(f, 'Target')
    plt.legend(loc='best')

    plt.title("Data set of size {1}".format(steps, 20))
    plt.xlabel("X1 (first dimension)")
    plt.ylabel("X2 (second dimension)")
    plt.savefig('p6_a.png')
    plt.clf()

    final_plot(data, 20, steps, target_weights, final, 'p6_b')

    for i in [(20, 'p6_c'), (100, 'p6_d'), (1000, 'p6_e')]:
        data = gen_data(i[0])
        final, steps = pla(data, target_weights)
        final_plot(data, i[0], steps, target_weights, final, i[1])
        print("Finished size=" + str(i[0]))

if __name__ == '__main__':
    main()
