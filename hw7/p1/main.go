package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gonum/matrix/mat64"
	"log"
	"os"
	"strings"
)

var (
	trainingFile string
	testingFile  string
	transform    bool
	pipe         bool

	outStream *os.File
)

func init() {
	flag.StringVar(&trainingFile, "i", "", "Training input file with data")
	flag.StringVar(&testingFile, "t", "", "Testing input file with data")
	flag.BoolVar(&transform, "3", false, "Set to true to use a 3rd order polynomial transoform")
	flag.BoolVar(&pipe, "p", false, "Set to pipe outputs to stdout")
}

func main() {
	flag.Parse()

	if trainingFile == "" || testingFile == "" {
		log.Fatal("Must pass an input file with -i and -t")
	}

	if pipe {
		outStream = os.Stdout
	} else {
		outStream, _ = os.Open(os.DevNull)
	}

	training_data, err := readDataFromFile(trainingFile)
	if err != nil {
		log.Fatal(err)
	}

	testing_data, err := readDataFromFile(testingFile)
	if err != nil {
		log.Fatal(err)
	}

	xs, ys := constructMatrices(training_data)
	xs_test, ys_test := constructMatrices(testing_data)
	if transform {
		xs = thirdOrder(xs)
		xs_test = thirdOrder(xs_test)
	}
	weights := linearRegression(xs, ys)

	training_digits_json, err := getIntensitySymmetryData(training_data)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(outStream, "%s\n", training_digits_json)

	testing_digits_json, err := getIntensitySymmetryData(testing_data)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(outStream, "%s\n", testing_digits_json)

	// Compute Ein on the testing and training data
	fmt.Printf("%v\n", sampleError(xs, ys, weights))
	fmt.Printf("%v\n", sampleError(xs_test, ys_test, weights))
}

func readDataFromFile(inputFile string) ([]*Digit, error) {
	var err error
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Failed to read file %s", inputFile)
	}

	digits := []*Digit{}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		digit, err := readNewDigit(strings.NewReader(scanner.Text()))
		if err != nil {
			return nil, err
		}

		digits = append(digits, digit)
	}

	return digits, nil
}

func constructMatrices(digits []*Digit) (*mat64.Dense, *mat64.Vector) {
	num := len(digits)

	// Create the matrices of x's and y's
	xs := make([]float64, 0, num)
	ys := make([]float64, 0, num)
	for i := range digits {
		if digits[i].digit == 1 {
			ys = append(ys, 1)
		} else if digits[i].digit == 5 {
			ys = append(ys, -1)
		} else {
			continue
		}
		xs = append(xs, 1)
		xs = append(xs, digits[i].averageIntensity())
		xs = append(xs, digits[i].horizontalSymmetry())
	}

	return mat64.NewDense(len(ys), 3, xs), mat64.NewVector(len(ys), ys)
}

func getIntensitySymmetryData(digits []*Digit) ([]byte, error) {
	type IntensitySymmetryResult struct {
		Digit     int
		Intensity float64
		Symmetry  float64
	}
	var res []*IntensitySymmetryResult

	for i := range digits {
		res = append(res, &IntensitySymmetryResult{
			Digit:     digits[i].digit,
			Intensity: digits[i].averageIntensity(),
			Symmetry:  digits[i].horizontalSymmetry(),
		})
	}

	return json.Marshal(res)
}
