package main

import (
	"github.com/gonum/matrix/mat64"
	"math"
)

func thirdOrder(mat mat64.Matrix) *mat64.Dense {
	// Assume two features

	rows, _ := mat.Dims()
	res := mat64.NewDense(rows, 10, nil)

	for i := 0; i < rows; i++ {
		x1 := mat.At(i, 1)
		x2 := mat.At(i, 2)

		new_row := []float64{1, x1, x2, math.Pow(x1, 2), x1 * x2, math.Pow(x2, 2),
			math.Pow(x1, 3), math.Pow(x1, 2) * x2, x1 * math.Pow(x2, 2), math.Pow(x2, 3)}

		res.SetRow(i, new_row)
	}

	return res
}
