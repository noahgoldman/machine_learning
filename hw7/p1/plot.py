import sys
import json

import numpy as np
import matplotlib.pyplot as plt

def create_polynomial_lines(w):
    xs = np.linspace(0, 0.6, 10000)

    ys = []
    for x in xs:
        ys.append(np.roots([
            w[9],
            w[8]*x + w[5],
            w[7]*(x**2) + w[4]*x + w[2],
            w[6]*(x**3) + w[3]*(x**2) + w[1]*x + w[0],
        ]))

    arr = np.array(ys)

    for col in arr.transpose():
        plt.scatter(xs, col, c='blue', edgecolor='none', s=5)

def plot_data_with_weights(weights, data):
    create_polynomial_lines(weights)

    ones = np.array([[x['Intensity'], x['Symmetry']] for x in data if x['Digit'] == 1])
    fives = np.array([[x['Intensity'], x['Symmetry']] for x in data if x['Digit'] == 5])

    plt.scatter(ones[:,0], ones[:,1], marker='o', s=40, c='blue')
    plt.scatter(fives[:,0], fives[:,1], marker='x', s=40, c='red')

    plt.ylim(top=1)
    plt.title("Features and third-order polynomial separator for images of the digits '1', and '5'")
    plt.xlabel("Average Intensity")
    plt.ylabel("Symmetry")
    plt.show()
    plt.clf()

    print("len is " + str(len(ones)+len(fives)))

if __name__ == '__main__':
    inputs = sys.stdin.readlines()

    weights = json.loads(inputs[0])
    training = json.loads(inputs[1])
    testing = json.loads(inputs[2])

    plot_data_with_weights(weights, training)
    plot_data_with_weights(weights, testing)

    print("Ein_training={}\tEin_test={}".format(inputs[3], inputs[4]))
