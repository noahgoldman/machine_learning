package main

import (
	"encoding/json"
	"fmt"
	"github.com/gonum/matrix/mat64"
	"log"
)

func linearRegression(xs mat64.Matrix, ys mat64.Matrix) *mat64.Vector {
	weights := linRegWeights(xs, ys)
	weights = pocket(xs, ys, weights, 1000)

	weights_slice := vectorToArray(weights)
	res, err := json.Marshal(weights_slice)
	if err != nil {
		log.Fatal("Failed to marshal: %s", err)
	}
	fmt.Fprintf(outStream, "%s\n", res)

	return weights
}

func linRegWeights(xs mat64.Matrix, ys mat64.Matrix) *mat64.Vector {
	x1 := new(mat64.Dense)
	x1.Mul(xs.T(), xs)

	x1_inv := new(mat64.Dense)
	err := x1_inv.Inverse(x1)
	if err != nil {
		log.Fatalf("Failed to invert matrix: %s", err)
	}

	x2 := new(mat64.Dense)
	x2.Mul(x1_inv, xs.T())

	res := new(mat64.Dense)
	res.Mul(x2, ys)

	return res.ColView(0)
}

func pocket(xs mat64.Matrix, ys mat64.Matrix, weights *mat64.Vector, iterations int) *mat64.Vector {
	for i := 0; i < iterations; i++ {
		new_weights := pocketPLA(xs, ys, weights)
		if new_weights == nil {
			return weights
		}
	}

	return weights
}

func pocketPLA(xs_mat mat64.Matrix, ys mat64.Matrix, weights *mat64.Vector) *mat64.Vector {
	xs := mat64.DenseCopyOf(xs_mat)
	rows, _ := xs.Dims()

	for i := 0; i < rows; i++ {
		vec := xs.RowView(i)

		if hypothesis(vec, weights) != ys.At(i, 0) {
			// Compute new weights
			new_weights := new(mat64.Vector)
			new_weights.ScaleVec(ys.At(i, 0), vec)
			new_weights.AddVec(weights, new_weights)

			if sampleError(xs, ys, new_weights) < sampleError(xs, ys, weights) {
				return new_weights
			}
		}
	}

	return nil
}

func sampleError(xs_mat mat64.Matrix, ys mat64.Matrix, weights *mat64.Vector) float64 {
	var errors float64 = 0
	xs := mat64.DenseCopyOf(xs_mat)

	rows, _ := xs.Dims()
	for i := 0; i < rows; i++ {
		vec := xs.RowView(i)

		if hypothesis(vec, weights) != ys.At(i, 0) {
			errors += 1
		}
	}

	return errors / float64(rows)
}

func hypothesis(x *mat64.Vector, w *mat64.Vector) float64 {
	product := new(mat64.Dense)
	product.Mul(w.T(), x)
	return signf(product.At(0, 0))
}
