package main

import (
	"testing"
)

func TestParseInput(t *testing.T) {
	t.Logf("yea..")
	data, err := readDataFromFile("ZipDigits.train")
	if err != nil {
		t.Fatal(err)
	}

	expected := 7291
	if len(data) != expected {
		t.Errorf("Read %d digits instead of %d", len(data), expected)
	}
}
