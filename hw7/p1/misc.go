package main

import (
	"github.com/gonum/matrix/mat64"
)

func sign(n int) int {
	return int(signf(float64(n)))
}

func signf(n float64) float64 {
	if n > 0 {
		return 1
	} else if n < 0 {
		return -1
	}
	return 0
}

func vectorToArray(vec *mat64.Vector) []float64 {
	rows, _ := vec.Dims()
	res := make([]float64, rows, rows)

	for i := 0; i < rows; i++ {
		res[i] = vec.At(i, 0)
	}

	return res
}
