\documentclass{article}



\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}

\begin{document}

\title{Homework 7}
\author{Noah Goldman}
\maketitle

\section{Classifying Handwritten Digits: 1 vs. 5}

\subsection{}

The first classification algorithm "Linear Regression for classification followed by pocket for improvement" was implemented, and ran on both the training and test data sets.  The algorithm utilized two features: average intensity and average symmetry.  The data sets are plotted below, along with the determined linear separator.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/p1_training}
    \caption{Training data set plotted with a linear separator for 1's and 5's}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/p1_test}
    \caption{Testing data set plotted with a linear separator for 1's and 5's}
\end{figure}

\subsection{}

The in-sample error was computed for the given linear separator on both the training and testing data sets as $E_{in}=0.019218449711723255$ and $E_{test}=0.04009433962264151$.

\subsection{}

We showed in Exercise 2.4 that the VC dimension of a linear classifier with $d$ dimensions is $d_{VC}=d+1$.  We can then use the generalization bound $E_{out}(g) \leq E_{in}(g) + \sqrt{\frac{8}{N}ln(\frac{4((2N)^{d_{VC}}+1)}{\delta}}$.  Letting $\delta=0.05$ and using the values of $E_{in}$ and $E_{test}$ given in the previous part will allow us to bound $E_{out}$ based on both the training and testing data sets.
\\

From the training data set with $N=1561$:

\begin{align*}
    E_{out}(g) &\leq E_{in}(g) + \sqrt{\frac{8}{N}ln(\frac{4((2N)^{d_{VC}}+1)}{\delta}} \\
    &\leq 0.019218449711723255 + \sqrt{\frac{8}{1561}ln(\frac{4((2*1561)^{2+1}+1)}{0.05}} \\
    &\leq 0.019218449711723255 + 0.382317 \\
    &\leq 0.401536
\end{align*}

From the test data set with $N=424$:

\begin{align*}
    E_{out}(g) &\leq E_{test}(g) + \sqrt{\frac{8}{N}ln(\frac{4((2N)^{d_{VC}}+1)}{\delta}} \\
    &\leq 0.04009433962264151 + \sqrt{\frac{8}{424}ln(\frac{4((2*424)^{2+1}+1)}{0.05}} \\
    &\leq 0.04009433962264151 + 0.681434 \\
    &\leq 0.721528
\end{align*}

As shown in the calculations above, the training set produced $E_{out} \leq 0.401536$ and the test set produced $E_{out} \leq 0.721528$.  As the bound based on $E_{in}$ is much closer to $E_{out} \leq 0$ than that produced by $E_{test}$, the bound from $E_{in}$ is better.

\subsection{}

The same classification algorithm was used again on the same training data set, but using a third-order polynomial transformed version of the input.  The same features were used, and the resulting third-order classifier is shown below in Figures \ref{poly_training} and \ref{poly_testing}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/p1_poly_training}
    \caption{Training data set plotted with a third-order separator for 1's and 5's}
    \label{poly_training}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/p1_poly_test}
    \caption{Testing data set plotted with a third-order separator for 1's and 5's}
    \label{poly_testing}
\end{figure}

The in-sample error was computed for the given third-order classfier on both the training and testing data sets as $E_{in}=0.01985906470211403$ and $E_{test}=0.04245283018867924$. Using the same reasoning found in part (c), bounds were computed for $E_{out}(g)$ using $E_{in}(g)$ and $E_{test}(g)$.  Using the formula $\tilde{d}=\frac{Q(Q+3)}{2}$ to compute the dimensions $\tilde{d}$ of a Qth order polynomial transform from two dimensions, we find that $\tilde{d}=9$.  This, along with $d_{VC}=\tilde{d}+1=10$ is used in the subsequent calculations.
\\

From the training data set with $N=1561$:

\begin{align*}
    E_{out}(g) &\leq E_{in}(g) + \sqrt{\frac{8}{N}ln(\frac{4((2N)^{d_{VC}}+1)}{\delta}} \\
    &\leq 0.01985906470211403 + \sqrt{\frac{8}{1561}ln(\frac{4((2*1561)^{9+1}+1)}{0.05}} \\
    &\leq 0.01985906470211403 + 0.659409 \\
    &\leq 0.679268
\end{align*}

From the testing data set with $N=424$:

\begin{align*}
    E_{out}(g) &\leq E_{in}(g) + \sqrt{\frac{8}{N}ln(\frac{4((2N)^{d_{VC}}+1)}{\delta}} \\
    &\leq 0.04245283018867924 + \sqrt{\frac{8}{424}ln(\frac{4((2*424)^{9+1}+1)}{0.05}} \\
    &\leq 0.04245283018867924 + 1.16401 \\
    &\leq 1.20646
\end{align*}

As shown in the results above, the bound based on $E_{in}$ is much tighter than that of $E_{test}$, which is greater than 1.

\subsection{}

If this model was being delivered to a customer, I would absolutely use the linear model without the 3rd order polynomial transform.  While the in-sample and test error proportions are effectively the same, the generalization bound shows that $E_{out}$ is likely to be much higher when using the polynomial transform.  Since the computed bounds in part (c) are much less than those in part (d), and the final goal of the algorithm is to achieve a low $E_{out}$, I would choose the linear model without the transform.

\section{Gradient Descent on a "Simple" Function}

\subsection{}

In order to perform gradient descent, the gradient of $f(x, y)=x^2+2y^2+2sin(2\pi x)sin(2\pi y)$ must be computed.

\begin{align*}
    \nabla(f(x, y)) &= \nabla(x^2+2y^2+2sin(2\pi x)sin(2\pi y)) \\
    &= \frac{df}{dx}[x^2+2y^2+2sin(2\pi x)sin(2\pi y)]\hat{x} + \frac{df}{dy}[x^2+2y^2+2sin(2\pi x)sin(2\pi y)]\hat{y} \\
    &= 2(x+2\pi sin(2\pi y)cos(2\pi x))\hat{x} + 4(y + \pi sin(2\pi x)cos(2\pi y))\hat{y}
\end{align*}

This gradient can be used to compute the new values of $x$ and $y$ for each iteration of the gradient descent algorithm.  Shown below are plots of the function value against iterations for gradient descent.  In both plots the algorithm starts at (0.1, 0.1) with 50 iterations, but one uses a learning rate of $\eta=0.01$ and the other uses $\eta=0.1$.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/p2a_1}
    \caption{Gradient descent starting at (0.1, 0.1) for 50 iterations with $\eta=0.01$}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/p2a_2}
    \caption{Gradient descent starting at (0.1, 0.1) for 50 iterations with $\eta=0.1$}
\end{figure}

\subsection{}

The following table was created by running gradient descent against the various points with 50 iterations, and a learning rate of $\eta=0.01$.

\begin{table}[h]
    \begin{tabular}{l|l|l}
        \bf{Initial Point} & \bf{Minimum Location} & \bf{Minimum Value} \\ \hline
        (0.1, 0.1) & (-0.243805, 0.237926) & -1.82007854155 \\
        (1, 1) & (1.21807, 0.712812) & 0.593269374326 \\
        (-0.5, -0.5) & (-0.731377, -0.237855) & -1.33248106233 \\
        (-1, -1) & (-1.21807, -0.712812) & 0.593269374326 \\
    \end{tabular}
\end{table}


\section{Problem 3.16}

\subsection{}

The final hypothesis $g(x)$ can be interpreted as the probability that the fingerprint $x$ should result in an "accept" outcome ($y=1$).  Therefore, the expected cost of accepting a person can be described as the cost of accepting times the probability that the person is accepted, plus the cost of denying times the probability that the person is denied.

\begin{align*}
    cost(accept) &= 0*P[accept] + c_aP[deny] \\
    &= 0*g(x) + c_a(1-g(x)) \\
    cost(accept) &= (1-g(x))c_a
\end{align*}

As shown above, we can say that the probability that any person is denied is $1-g(x)$.  This results in the expected cost of accepting a person being $cost(accept)=(1-g(x))c_a$.  We can use a similar process to find the expected cost of denying a person to be $cost(deny)=g(x)c_r$, as shown below.

\begin{align*}
    cost(deny) &= c_rP[accept] + 0*P[deny] \\
    &= c_rg(x) + 0*(1-g(x)) \\
    cost(deny) &= g(x)c_r
\end{align*}

Again, we use the fact that the probability of accepting a person is $g(x)$ to compute the expected value.

\subsection{}

The values computed in part (a) are clearly the expected values of the costs of either accepting, or denying any particular person based on their fingerprint.  However, these expected values vary based on the input $x$.  In general, the condition for accepting any person should be when $cost(accept) > cost(deny)$ (or $(1-g(x))c_a > g(x)c_r$).  Therefore, the threshold value of $g(x)=\kappa$ can be computed when $cost(accept)=cost(deny)=(1-\kappa)c_a=\kappa c_r$.

\begin{align*}
    (1-\kappa)c_a &= \kappa c_r \\
    c_a - \kappa c_a &= \\
    c_a &= \kappa(c_a + c_r) \\
    \kappa &= \frac{c_a}{c_a + c_r}
\end{align*}

As shown above, the threshold can be found as $\kappa = \frac{c_a}{c_a + c_r}$.

\subsection{}

In the case of the Supermarket in Example 1.1, the threshold is computed as $\kappa=\frac{1}{1+10}=\frac{1}{11}$ where $c_a=1$ and $c_r=10$.  The threshold is low, but this seems reasonable for the application.  Since the damage caused by inadvertantly rejecting a person is relatively high due to the fact that they will likely be upset at the store, and the downside of accidentally accepting someone is low as they'll just recieve a coupon, it makes intuitive sense that the threshold for accepting a fingerprint should be low.

For the CIA (from Example 1.1), the threshold is found to be $\kappa=\frac{1000}{1000+1}=\frac{1000}{1001}=0.999$ where $c_a=1000$ and $c_r=1$.  As the potential risk resulting from accepting the wrong person is very high, it makes a lot of sense that the threshold would be nearly one.  Clearly, access should only be granted if the hypothesis is absolutely sure the fingerprint should be accepted.

\end{document}
