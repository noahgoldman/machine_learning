import math
import sys

import matplotlib.pyplot as plt

def x_deriv(x, y):
    return 2*(x + 2*math.pi*math.sin(2*math.pi*y)*math.cos(2*math.pi*x))

def y_deriv(x, y):
    return 4*(y + math.pi*math.sin(2*math.pi*x)*math.cos(2*math.pi*y))

def f_x_y(x, y):
    return x**2 + 2*(y**2) + 2*math.sin(2*math.pi*x)*math.sin(2*math.pi*y)

def gradient_descent(x, y, iterations, rate, plot=False):
    vals = []
    vals.append(f_x_y(x, y))

    for _ in range(iterations):
        x -= rate*x_deriv(x, y)
        y -= rate*y_deriv(x, y)

        vals.append(f_x_y(x, y))

    if plot:
        plt.plot(range(len(vals)), vals)

        plt.xlabel("Iterations")
        plt.ylabel("Value of f(x, y)")
        plt.title("Function values from gradient descent with N={} iterations"
                .format(iterations))
        plt.show()

    return x, y

if __name__ == '__main__':
    _, x, y, iterations, rate, plot = sys.argv
    final_x, final_y = gradient_descent(float(x), float(y), int(iterations),
            float(rate), plot == 'true')

    print("Minimized to ({}, {}) with a value of {}"
            .format(round(final_x, 6), round(final_y, 6), f_x_y(final_x, final_y)))
