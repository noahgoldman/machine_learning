package main

import (
	"github.com/gonum/matrix/mat64"
	"log"
)

// Legendre transform for two features
func legendreTransform(mat mat64.Matrix, dim int) *mat64.Dense {
	rows, cols := mat.Dims()

	if cols != 3 {
		log.Fatal("Legendre transform needs exactly two features")
	}

	res := mat64.NewDense(rows, numPolynomialDimensions(dim), nil)

	for i := 0; i < rows; i++ {
		res.SetRow(i, legendreTransformSingle(mat.At(i, 1), mat.At(i, 2), dim))
	}

	return res
}

func legendreTransformSingle(x1 float64, x2 float64, dim int) []float64 {
	return polynomialTransform(computeLegendre(x1, dim), computeLegendre(x2, dim))
}

func computeLegendre(in float64, dims int) []float64 {
	res := make([]float64, dims+1)
	res[0] = 1
	res[1] = in

	if dims < 2 {
		return res
	}
	for i := 2; i <= dims; i++ {
		res[i] = nextLegendre(float64(i), in, res[i-1], res[i-2])
	}

	return res
}

func nextLegendre(k float64, x float64, x1 float64, x2 float64) float64 {
	return (((2*k - 1) / k) * x * x1) - (((k - 1) / k) * x2)
}

func polynomialTransform(x1s []float64, x2s []float64) []float64 {
	if len(x1s) != len(x2s) {
		log.Fatal("The two input arrays must be of the same size")
	}

	return polynomialTransformDim(x1s, x2s, len(x1s))
}

func polynomialTransformDim(x1s []float64, x2s []float64, dim int) []float64 {
	res := make([]float64, 0, numPolynomialDimensions(dim))

	if dim > len(x1s) || dim < len(x2s) {
		log.Fatal("The inputs to polynomial transform don't have enough dimensions")
	}

	for i := 0; i < len(x1s); i++ {
		for j := 0; i+j < dim; j++ {
			res = append(res, x1s[i]*x2s[j])
		}
	}

	return res
}

func numPolynomialDimensions(dim int) int {
	return (dim*(dim+3))/2 + 1
}
