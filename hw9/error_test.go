package main

import (
	"github.com/gonum/matrix/mat64"
	"testing"
)

func TestSampleError(t *testing.T) {
	xs := mat64.NewDense(2, 3, []float64{1, 1, 1, 1, -2, 1})
	ys := mat64.NewVector(2, []float64{1, 1})
	w := mat64.NewVector(3, []float64{0, 1, 1})

	res := sampleError(xs, ys, w)
	if res != 0.5 {
		t.Errorf("The sample error should be 0.5, not %d", res)
	}
}
