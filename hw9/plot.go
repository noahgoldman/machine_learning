package main

import (
	"fmt"
	"github.com/gonum/matrix/mat64"
	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg/draw"
	"image/color"
)

const (
	PLOTTER_GRID_DIM = 1000

	IMG_WIDTH  = 600
	IMG_HEIGHT = 400
)

func plotWeights(w *mat64.Vector, dim int, digits []*Digit, reg float64) {
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = fmt.Sprintf("Decision boundary and training data for lambda=%v", reg)
	p.X.Label.Text = "Normalized average intensity"
	p.Y.Label.Text = "Normalized horizontal symmetry"

	p.Add(plotter.NewGrid())

	weights_scatter, err := plotter.NewScatter(makePointsWeights(w, dim))
	if err != nil {
		panic(err)
	}
	weights_scatter.GlyphStyle.Color = color.RGBA{R: 153, G: 255, B: 153, A: 255}
	weights_scatter.GlyphStyle.Shape = draw.CircleGlyph{}

	ones, others := makePointsDigits(digits)

	ones_scatter, err := plotter.NewScatter(ones)
	if err != nil {
		panic(err)
	}
	ones_scatter.GlyphStyle.Color = color.RGBA{B: 255, A: 255}

	other_scatter, err := plotter.NewScatter(others)
	if err != nil {
		panic(err)
	}
	other_scatter.GlyphStyle.Color = color.RGBA{R: 255, A: 255}
	other_scatter.GlyphStyle.Shape = draw.CrossGlyph{}

	p.Add(weights_scatter, other_scatter, ones_scatter)

	if err := p.Save(IMG_WIDTH, IMG_HEIGHT, "out.png"); err != nil {
		panic(err)
	}
}

func plotCrossValErrors(ecv []float64, etest []float64) {
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = "Plot of cross validation and testing set errors"
	p.X.Label.Text = "Regularization parameter (lambda)"
	p.Y.Label.Text = "Error (E_cv or E_test)"

	p.Add(plotter.NewGrid())

	// Make the points for each set
	ecv_pts := make(plotter.XYs, len(ecv)-1)
	etest_pts := make(plotter.XYs, len(etest)-1)
	for i := 1; i < len(ecv); i++ {
		j := i - 1
		ecv_pts[j].X = float64(i) / 100
		etest_pts[j].X = ecv_pts[j].X

		ecv_pts[j].Y = ecv[i]
		etest_pts[j].Y = etest[i]
	}

	ecv_line, ecv_points, err := plotter.NewLinePoints(ecv_pts)
	if err != nil {
		panic(err)
	}
	ecv_line.Color = color.RGBA{G: 255, A: 255}
	ecv_points.Color = color.RGBA{G: 255, A: 255}

	etest_line, etest_points, err := plotter.NewLinePoints(etest_pts)
	if err != nil {
		panic(err)
	}
	etest_line.Color = color.RGBA{R: 255, A: 255}
	etest_points.Color = color.RGBA{R: 255, A: 255}

	p.Add(etest_line, etest_points, ecv_line, ecv_points)
	p.Legend.Add("Cross validation error", ecv_line, ecv_points)
	p.Legend.Add("Testing error", etest_line, etest_points)

	if err := p.Save(IMG_WIDTH, IMG_HEIGHT, "val.png"); err != nil {
		panic(err)
	}
}

func makePointsDigits(digits []*Digit) (plotter.XYs, plotter.XYs) {
	plus := make(plotter.XYs, 0, len(digits))
	minus := make(plotter.XYs, 0, len(digits))
	for i := range digits {
		new_pt := NewPoint(digits[i].averageIntensity(), digits[i].horizontalSymmetry())
		if digits[i].digit == 1 {
			plus = append(plus, new_pt)
		} else {
			minus = append(minus, new_pt)
		}
	}
	return plus, minus
}

func makePointsWeights(w *mat64.Vector, dim int) plotter.XYs {
	pts := make(plotter.XYs, 0, PLOTTER_GRID_DIM*PLOTTER_GRID_DIM)
	res := new(mat64.Dense)

	var step float64 = float64(2) / PLOTTER_GRID_DIM
	for i := float64(-1); i < 1; i += step {
		for j := float64(-1); j < 1; j += step {
			transformed := legendreTransformSingle(i, j, dim)
			res.Mul(w.T(), mat64.NewVector(len(transformed), transformed))

			if res.At(0, 0) > 0 {
				pts = append(pts, NewPoint(i, j))
			}
		}
	}
	return pts
}

type Point struct {
	X float64
	Y float64
}

func NewPoint(x float64, y float64) Point {
	return Point{X: x, Y: y}
}
