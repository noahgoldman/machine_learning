package main

import (
	"bufio"
	"flag"
	"github.com/gonum/matrix/mat64"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"
)

const (
	NUM_TRANSFORM_DIMS = 8

	OPTIMIZE_WIDTH = 201
)

var (
	trainingFile string
	testingFile  string
	transform    bool
	printOutput  bool
	regParam     float64
	optimize     bool
)

func init() {
	flag.StringVar(&trainingFile, "i", "", "Training input file with data")
	flag.StringVar(&testingFile, "t", "", "Testing input file with data")
	flag.Float64Var(&regParam, "reg", 0, "Regularization parameters")
	flag.BoolVar(&printOutput, "p", false, "Set to pipe outputs to stdout")

	flag.BoolVar(&optimize, "op", false,
		"Set to run the optimization for the regularization param")
}

func main() {
	flag.Parse()

	if trainingFile == "" || testingFile == "" {
		log.Fatal("Must pass an input and testing file with -i and -t")
	}

	training_data, err := readDataFromFile(trainingFile)
	if err != nil {
		log.Fatal(err)
	}

	testing_data, err := readDataFromFile(testingFile)
	if err != nil {
		log.Fatal(err)
	}

	all_data := append(training_data, testing_data...)
	setNormalizationFromWeights(all_data)

	xs, ys := constructMatrices(training_data)
	xs = legendreTransform(xs, NUM_TRANSFORM_DIMS)

	xs_test, ys_test := constructMatrices(testing_data)
	xs_test = legendreTransform(xs_test, NUM_TRANSFORM_DIMS)

	if optimize {
		optimizeRegularization(xs, ys, xs_test, ys_test)
	} else {
		classifyAndPlot(xs, ys, xs_test, ys_test, training_data)
	}
}

func classifyAndPlot(xs mat64.Matrix, ys mat64.Matrix, xs_test mat64.Matrix,
	ys_test mat64.Matrix, training_data []*Digit) {

	weights := linRegWeightsReg(xs, ys, regParam)

	printVectorJSON(weights)
	log.Printf("Computed weights with lambda=%v resulting in Etest=%v",
		regParam, sampleError(xs_test, ys_test, weights))
	plotWeights(weights, NUM_TRANSFORM_DIMS, training_data, regParam)
}

func optimizeRegularization(xs mat64.Matrix, ys mat64.Matrix,
	xs_test mat64.Matrix, ys_test mat64.Matrix) {

	ecv := make([]float64, OPTIMIZE_WIDTH)
	etest := make([]float64, OPTIMIZE_WIDTH)

	// Maximal lambda calculation
	minIndex := 0

	var wg sync.WaitGroup
	procs := runtime.GOMAXPROCS(0)
	wg.Add(procs)
	c := make(chan int)

	for i := 0; i < procs; i++ {
		go func() {
			defer wg.Done()
			for n := range c {
				reg := float64(n) / 100
				ecv[n] = crossValError(xs, ys, reg)

				weights := linRegWeightsReg(xs, ys, reg)
				etest[n] = sampleError(xs_test, ys_test, weights)

				if ecv[n] < ecv[minIndex] {
					minIndex = n
				}
			}
		}()
	}

	for i := range ecv {
		c <- i
	}
	close(c)
	wg.Wait()

	log.Printf("Found lambda*=%v with Ecv=%v", float64(minIndex)/100, ecv[minIndex])

	plotCrossValErrors(ecv, etest)
}

func readDataFromFile(inputFile string) ([]*Digit, error) {
	var err error
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Failed to read file %s", inputFile)
	}

	digits := []*Digit{}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		digit, err := readNewDigit(strings.NewReader(scanner.Text()))
		if err != nil {
			return nil, err
		}

		digits = append(digits, digit)
	}

	return digits, nil
}

func constructMatrices(digits []*Digit) (*mat64.Dense, *mat64.Vector) {
	num := len(digits)

	// Create the matrices of x's and y's
	xs := make([]float64, num*3)
	ys := make([]float64, num)
	for i := range digits {
		if digits[i].digit == 1 {
			ys[i] = 1

		} else {
			ys[i] = -1
		}
		cur := 3 * i
		xs[cur] = 1
		xs[cur+1] = digits[i].averageIntensity()
		xs[cur+2] = digits[i].horizontalSymmetry()
	}

	return mat64.NewDense(num, 3, xs), mat64.NewVector(num, ys)
}
