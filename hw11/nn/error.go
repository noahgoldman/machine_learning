package nn

func SampleErrorK(training []*Point, testing []*Point, k int) float64 {
	errors := 0
	for i := range testing {
		if testing[i].RunKNN(training, k) != testing[i].Class {
			errors++
		}
	}
	return float64(errors) / float64(len(testing))
}

func LeaveOneCrossValidation(training []*Point, k int) float64 {
	errors := 0
	for i := range training {
		train := append(training[:i], training[i+1:]...)
		if training[i].RunKNN(train, k) != training[i].Class {
			errors++
		}
	}

	return float64(errors) / float64(len(training))
}

func CrossValidationFold(training []*Point, k int, K int) float64 {
	folds := makeFolds(training, K)

	errors_percent := float64(0)
	for i := range folds {
		rest := make([]*Point, 0, len(training))
		for j := range folds {
			if j != i {
				rest = append(rest, folds[j]...)
			}
		}

		errors_percent += SampleErrorK(rest, folds[i], k)
	}

	return errors_percent / float64(K)
}

func makeFolds(pts []*Point, K int) [][]*Point {
	folds := make([][]*Point, K)
	shufflePoints(pts)

	j := 0
	for i := range pts {
		folds[j] = append(folds[j], pts[i])

		j++
		if j == len(folds) {
			j = 0
		}
	}

	return folds
}
