package nn

import (
	"fmt"
	"math"
	"sort"

	"../util"
)

type Point struct {
	Coords []float64
	Class  int
}

type Points []*Point

func (pts Points) Len() int { return len(pts) }
func (pts Points) XY(i int) (float64, float64) {
	pt := pts[i]
	if len(pt.Coords) != 2 {
		panic(fmt.Errorf("The Points type shouldn't be used unless there are only 2 dims"))
	}

	return pt.Coords[0], pt.Coords[1]
}

func (pts Points) farthest(others Points) *Point {
	distances := make([]float64, len(others))
	for i := range others {
		closest := others[i].sortByDistance(pts)[0]
		distances[i] = others[i].Distance(closest)
	}

	_, max_i := util.MaxFloat(distances)
	return &Point{Coords: others[max_i].Coords}
}

func (pts Points) average() (*Point, error) {
	if len(pts) == 0 {
		return nil, fmt.Errorf("The points array had a size of zero")
	}

	coord := make([]float64, len(pts[0].Coords))
	for i := range pts {
		for j := range pts[i].Coords {
			coord[j] += pts[i].Coords[j]
		}
	}

	for j := range coord {
		coord[j] /= float64(len(pts))
	}

	return &Point{Coords: coord}, nil
}

func (p *Point) RunKNN(points []*Point, k int) int {
	if len(points) < k {
		panic(fmt.Errorf("The input points to runKNN (length %d) should be at least k=%d",
			len(points), k))
	}
	if k%2 == 0 {
		panic(fmt.Errorf("K must be odd, (given k=%d)", k))
	}

	pts := p.sortByDistance(points)

	classes := make(map[int]int)
	for i := range pts[:k] {
		classes[pts[i].Class] += 1
	}

	// Pick the max class and return it
	maxClass := 0
	maxCount := 0

	for k, v := range classes {
		if v > maxCount || (v == maxCount && k < maxClass) {
			maxCount = v
			maxClass = k
		}
	}

	return maxClass
}

func (p *Point) sortByDistance(points []*Point) []*Point {
	pts := append([]*Point(nil), points...)

	// Sort by distance
	sort.Sort(pointsByDistance{points: pts, center: p})

	return pts
}

// Compute the euclidean distance between two points
func (p *Point) Distance(pt *Point) float64 {
	if len(p.Coords) != len(pt.Coords) {
		panic(fmt.Errorf("The two points %v and %v aren't in the same number of dimensions",
			p, pt))
	}

	sum := 0.0
	for i := range p.Coords {
		sum += math.Pow(p.Coords[i]-pt.Coords[i], 2)
	}

	return math.Sqrt(sum)
}

// Types for sorting by distance
type pointsByDistance struct {
	points []*Point
	center *Point
}

func (b pointsByDistance) Len() int      { return len(b.points) }
func (b pointsByDistance) Swap(i, j int) { b.points[i], b.points[j] = b.points[j], b.points[i] }
func (b pointsByDistance) Less(i, j int) bool {
	return b.points[i].Distance(b.center) < b.points[j].Distance(b.center)
}
