package nn

import (
	"math/rand"
)

func MakeCenters(data []*Point, n int) []*Point {
	centers := make(Points, 1, n)
	centers[0] = genRandomPoint()

	// Compute the farthest point from the rest of the set and add it in
	for i := 1; i < n; i++ {
		centers = append(centers, centers.farthest(data))
		centers[i].Class = i
	}

	// Group all the points into regions based on their nearest center
	regions := make([]Points, n)
	for i := range data {
		j := data[i].RunKNN(centers, 1)
		regions[j] = append(regions[j], data[i])
	}

	// Find the centers using averages
	var err error
	for i := range regions {
		if len(regions[i]) != 0 {
			centers[i], err = regions[i].average()
			if err != nil {
				panic(err)
			}
		}
	}

	return centers
}

// Create a random point with coords in the range [-1, 1]
func genRandomPoint() *Point {
	coordRand := func() float64 {
		return rand.Float64()*2 - 1
	}
	return &Point{Coords: []float64{coordRand(), coordRand()}, Class: 0}
}
