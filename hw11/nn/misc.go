package nn

import (
	"math/rand"
)

func shufflePoints(pts []*Point) {
	for i := range pts {
		j := rand.Intn(i + 1)
		pts[i], pts[j] = pts[j], pts[i]
	}
}
