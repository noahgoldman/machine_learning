package nn

import (
	"math"
)

// Get 2d limits
func get2DLimits(pts []*Point) (*Point, *Point) {
	min := &Point{[]float64{0, 0}, 0}
	max := &Point{[]float64{0, 0}, 0}
	for i := range pts {
		min.Coords[0] = math.Min(min.Coords[0], pts[i].Coords[0])
		min.Coords[1] = math.Min(min.Coords[1], pts[i].Coords[1])

		max.Coords[0] = math.Max(max.Coords[0], pts[i].Coords[0])
		max.Coords[1] = math.Max(max.Coords[1], pts[i].Coords[1])
	}

	return min, max
}
