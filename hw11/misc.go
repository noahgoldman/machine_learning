package main

import (
	"encoding/json"
	"fmt"
	"github.com/gonum/matrix/mat64"
	"log"
	"os"
)

func vectorToArray(vec *mat64.Vector) []float64 {
	rows, _ := vec.Dims()
	res := make([]float64, rows, rows)

	for i := 0; i < rows; i++ {
		res[i] = vec.At(i, 0)
	}

	return res
}

func printVectorJSON(vec *mat64.Vector) {
	if !printOutput {
		return
	}

	arr := vectorToArray(vec)
	res, err := json.Marshal(arr)
	if err != nil {
		log.Fatal("Failed to marshal: %s", err)
	}
	fmt.Fprintf(os.Stdout, "%s\n", res)
}

func identityMat(n int) *mat64.Dense {
	mat := mat64.NewDense(n, n, nil)
	for i := 0; i < n; i++ {
		mat.Set(i, i, 1)
	}
	return mat
}
