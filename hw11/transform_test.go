package main

import (
	"fmt"
	"github.com/gonum/matrix/mat64"
	"testing"
)

func runLegendreTransformTest(rows int) error {
	cols := 3
	fakedata := make([]float64, rows*cols)
	for i := range fakedata {
		fakedata[i] = 1
	}

	mat := mat64.NewDense(rows, cols, fakedata)

	legendreTransform(mat, 10)

	// should be all ones
	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			if mat.At(i, j) != 1 {
				return fmt.Errorf("Expected 1, got %v", mat.At(i, j))
			}
		}
	}

	return nil
}

func TestLegendreTransform(t *testing.T) {
	err := runLegendreTransformTest(20)
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkLegendreTransform(b *testing.B) {
	runLegendreTransformTest(b.N)
}
