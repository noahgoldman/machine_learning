package main

import (
	"fmt"
	"image/color"

	"github.com/gonum/matrix/mat64"
	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg/draw"

	"./nn"
)

const (
	PLOTTER_GRID_DIM = 500

	IMG_WIDTH  = 600
	IMG_HEIGHT = 400
)

func plotEcv(ecv []float64, labels []float64) {
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = "Effect of k (number of centers) on RBF-network cross-validation errors"
	p.X.Label.Text = "k (Number of centers)"
	p.Y.Label.Text = "E_CV"

	p.Add(plotter.NewGrid())

	// Make points
	ecv_pts := make(plotter.XYs, len(ecv))
	for i := range ecv {
		ecv_pts[i].X = labels[i]
		ecv_pts[i].Y = ecv[i]
	}

	ecv_line, ecv_points, err := plotter.NewLinePoints(ecv_pts)
	if err != nil {
		panic(err)
	}

	p.Add(ecv_line, ecv_points)

	if err := p.Save(IMG_WIDTH, IMG_HEIGHT, "nn_val.png"); err != nil {
		panic(err)
	}
}

func plotNNBoundaries(points nn.Points, grid_dim int, k int, digits []*Digit) {
	grid_step := float64(2) / float64(grid_dim)

	ones := make(nn.Points, 0)
	for i := float64(-1); i <= 1; i += grid_step {
		for j := float64(-1); j <= 1; j += grid_step {
			pt := &nn.Point{Coords: []float64{i, j}}
			if pt.RunKNN(points, k) == 1 {
				ones = append(ones, pt)
			}
		}
	}

	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = "21-NN rule decision boundary with 300 training points"
	p.X.Label.Text = "Average Intensity (Normalized)"
	p.Y.Label.Text = "Horizontal Symmetry (Normalized)"

	region, err := plotter.NewScatter(ones)
	if err != nil {
		panic(err)
	}
	region.GlyphStyle.Color = color.RGBA{R: 153, G: 255, B: 153, A: 255}
	region.GlyphStyle.Shape = draw.CircleGlyph{}
	region.GlyphStyle.Radius = 3.5
	p.Add(region)

	plotDigits(p, digits)

	if err := p.Save(IMG_WIDTH, IMG_HEIGHT, "nn_boundary.png"); err != nil {
		panic(err)
	}
}

func plotWeights(w *mat64.Vector, transform transformFunc, digits []*Digit) {
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = fmt.Sprintf("Decision boundary and training data")
	p.X.Label.Text = "Normalized average intensity"
	p.Y.Label.Text = "Normalized horizontal symmetry"

	p.Add(plotter.NewGrid())

	weights_scatter, err := plotter.NewScatter(makePointsWeights(w, transform))
	if err != nil {
		panic(err)
	}
	weights_scatter.GlyphStyle.Color = color.RGBA{R: 153, G: 255, B: 153, A: 255}
	weights_scatter.GlyphStyle.Shape = draw.CircleGlyph{}
	p.Add(weights_scatter)

	plotDigits(p, digits)

	if err := p.Save(IMG_WIDTH, IMG_HEIGHT, "out.png"); err != nil {
		panic(err)
	}
}

func plotDigits(plt *plot.Plot, digits []*Digit) {
	ones, others := makePointsDigits(digits)

	ones_scatter, err := plotter.NewScatter(ones)
	if err != nil {
		panic(err)
	}
	ones_scatter.GlyphStyle.Color = color.RGBA{B: 255, A: 255}

	others_scatter, err := plotter.NewScatter(others)
	if err != nil {
		panic(err)
	}
	others_scatter.GlyphStyle.Color = color.RGBA{R: 255, A: 255}
	others_scatter.GlyphStyle.Shape = draw.CrossGlyph{}

	plt.Add(ones_scatter, others_scatter)
}

func makePointsDigits(digits []*Digit) (plotter.XYs, plotter.XYs) {
	plus := make(plotter.XYs, 0, len(digits))
	minus := make(plotter.XYs, 0, len(digits))
	for i := range digits {
		new_pt := NewPoint(digits[i].averageIntensity(), digits[i].horizontalSymmetry())
		if digits[i].digit == 1 {
			plus = append(plus, new_pt)
		} else {
			minus = append(minus, new_pt)
		}
	}
	return plus, minus
}

func makePointsWeights(w *mat64.Vector, transform transformFunc) plotter.XYs {
	pts := make(plotter.XYs, 0, PLOTTER_GRID_DIM*PLOTTER_GRID_DIM)
	res := new(mat64.Dense)

	var step float64 = float64(2) / PLOTTER_GRID_DIM
	for i := float64(-1); i < 1; i += step {
		for j := float64(-1); j < 1; j += step {
			transformed := transform(i, j)
			res.Mul(w.T(), mat64.NewVector(len(transformed), transformed))

			if res.At(0, 0) > 0 {
				pts = append(pts, NewPoint(i, j))
			}
		}
	}
	return pts
}

type Point struct {
	X float64
	Y float64
}

func NewPoint(x float64, y float64) Point {
	return Point{X: x, Y: y}
}
