package util

func Sign(n int) int {
	return int(Signf(float64(n)))
}

func Signf(n float64) float64 {
	if n > 0 {
		return 1
	} else if n < 0 {
		return -1
	}
	return 0
}

func minMaxFloat(arr []float64, comparison comparisonFunc) (float64, int) {
	m := arr[0]
	m_i := 0
	for i := 1; i < len(arr); i++ {
		if comparison(arr[i], m) {
			m = arr[i]
			m_i = i
		}
	}

	return m, m_i
}

func MaxFloat(arr []float64) (float64, int) {
	return minMaxFloat(arr, gt)
}

func MinFloat(arr []float64) (float64, int) {
	return minMaxFloat(arr, lt)
}

type comparisonFunc func(float64, float64) bool

func gt(l float64, r float64) bool {
	return l > r
}

func lt(l float64, r float64) bool {
	return l < r
}
