import os
import sys
import random

NUM_TEST = 300

def make_sets(path):
    _, fname = os.path.split(path)
    name, ext = os.path.splitext(fname)

    training = []
    test = []

    with open(path, 'r') as f:
        test = f.readlines()

    for _ in range(NUM_TEST):
        chosen = random.choice(test)
        training.append(chosen)
        test.remove(chosen)

    with open(name + '.train', 'w') as f:
        f.writelines(training)

    with open(name + '.test', 'w') as f:
        f.writelines(test)

if __name__ == '__main__':
    _, path = sys.argv
    make_sets(path)
