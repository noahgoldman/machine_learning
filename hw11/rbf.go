package main

import (
	"math"

	"github.com/gonum/matrix/mat64"

	"./nn"
)

func RBF(pts nn.Points, k int, r float64) (*mat64.Vector, nn.Points) {
	centers := nn.MakeCenters(pts, k)
	xs, ys := makeRBFMatrices(pts, centers, k, r)
	weights := linRegWeights(xs, ys)

	return weights, centers
}

func makeRBFMatrices(pts nn.Points, centers nn.Points, k int, r float64) (
	*mat64.Dense, *mat64.Vector) {

	return rbfMatrix(pts, centers, r), makeYs(pts)
}

// Create a matrix resulting from the RBF feature transform
func rbfMatrix(pts nn.Points, centers nn.Points, r float64) *mat64.Dense {
	arr := make([]float64, 0, len(pts)*(len(centers)+1))

	for i := range pts {
		arr = append(arr, rbfTransform(pts[i], centers, r)...)
	}

	return mat64.NewDense(len(pts), len(centers)+1, arr)
}

func rbfTransform(pt *nn.Point, centers nn.Points, r float64) []float64 {
	res := make([]float64, len(centers)+1)
	res[0] = 1
	for i := range centers {
		res[i+1] = gaussianKernel(pt.Distance(centers[i]) / r)
	}

	return res
}

func makeYs(pts nn.Points) *mat64.Vector {
	ys := make([]float64, len(pts))
	for i := range pts {
		ys[i] = float64(pts[i].Class)
	}
	return mat64.NewVector(len(ys), ys)
}

func gaussianKernel(z float64) float64 {
	return math.Exp(-1 * 0.5 * math.Pow(z, 2))
}
