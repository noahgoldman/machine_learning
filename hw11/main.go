package main

import (
	"bufio"
	"flag"
	"log"
	"math"
	"os"
	"strings"

	"./nn"
	"./util"
)

const (
	MAX_K_NN    = 60
	CHOSEN_K_NN = 27
	FOLDS       = 10
	NN_GRID_DIM = 200

	MAX_K_RBF    = 60
	CHOSEN_K_RBF = 32
)

var (
	trainingFile string
	testingFile  string
	model        string
	printOutput  bool
	crossVal     bool
)

func init() {
	flag.StringVar(&trainingFile, "i", "", "Training input file with data")
	flag.StringVar(&testingFile, "t", "", "Testing input file with data")
	flag.BoolVar(&printOutput, "p", false, "Set to pipe outputs to stdout")

	flag.StringVar(&model, "m", "", "The model to run")
	flag.BoolVar(&crossVal, "cv", false, "Run cross validation")
}

func main() {
	flag.Parse()

	if trainingFile == "" || testingFile == "" {
		log.Fatal("Must pass an input and testing file with -i and -t")
	}

	training_data, err := readDataFromFile(trainingFile)
	if err != nil {
		log.Fatal(err)
	}

	testing_data, err := readDataFromFile(testingFile)
	if err != nil {
		log.Fatal(err)
	}

	all_data := append(training_data, testing_data...)
	setNormalizationFromWeights(all_data)

	points := makeNNPoints(training_data)
	testing_pts := makeNNPoints(testing_data)

	if model == "nn" {
		if crossVal {
			knnCrossVal(points)
		} else {
			plotNN(points, testing_pts, training_data)
		}
	} else if model == "rbf" {
		if crossVal {
			rbfCrossVal(points)
		} else {
			rbf(points, testing_pts, training_data)
		}
	} else {
		log.Fatal("Enter a valid model to run")
	}
}

func plotNN(points nn.Points, testing_pts nn.Points, digits []*Digit) {
	plotNNBoundaries(points, NN_GRID_DIM, CHOSEN_K_NN, digits)

	log.Printf("Cross validation error=%v",
		nn.CrossValidationFold(points, CHOSEN_K_NN, FOLDS))
	log.Printf("Sample error=%v", nn.SampleErrorK(points, points, CHOSEN_K_NN))
	log.Printf("Test error=%v", nn.SampleErrorK(points, testing_pts, CHOSEN_K_NN))
}

func knnCrossVal(points nn.Points) {
	ecv := make([]float64, 0, MAX_K_NN)
	labels := make([]float64, 0, MAX_K_NN)
	for k := 1; k <= MAX_K_NN; k += 2 {
		ecv = append(ecv, nn.CrossValidationFold(points, k, FOLDS))
		labels = append(labels, float64(k))
	}

	min_value, min_index := util.MinFloat(ecv)
	log.Printf("The minimum value is k=%v with E=%v", labels[min_index], min_value)

	plotEcv(ecv, labels)
}

func rbf(points nn.Points, testing nn.Points, digits []*Digit) {
	k := CHOSEN_K_RBF
	r := float64(2) / math.Sqrt(float64(k))
	weights, centers := RBF(points, k, r)

	// Make the transform function
	transform := func(x, y float64) []float64 {
		return rbfTransform(&nn.Point{Coords: []float64{x, y}}, centers, r)
	}

	plotWeights(weights, transform, digits)

	xs, ys := makeRBFMatrices(points, centers, k, r)
	xs_test, ys_test := makeRBFMatrices(testing, centers, k, r)

	// Make matrices for the testing points
	log.Printf("Sample error=%v", sampleError(xs, ys, weights))
	log.Printf("E_test=%v", sampleError(xs_test, ys_test, weights))
}

func rbfCrossVal(points nn.Points) {
	ecv := make([]float64, 0, MAX_K_RBF)
	labels := make([]float64, 0, MAX_K_RBF)
	for k := 1; k <= MAX_K_RBF; k++ {
		r := float64(2) / math.Sqrt(float64(k))
		centers := nn.MakeCenters(points, k)
		xs, ys := makeRBFMatrices(points, centers, k, r)
		ecv = append(ecv, crossValErrorLinear(xs, ys, 0))
		labels = append(labels, float64(k))
	}

	min_value, min_index := util.MinFloat(ecv)
	log.Printf("The minimum value is k=%v with E=%v", labels[min_index], min_value)

	plotEcv(ecv, labels)
}

func makeNNPoints(digits []*Digit) []*nn.Point {
	points := make([]*nn.Point, len(digits))
	for i := range digits {
		class := -1
		if digits[i].digit == 1 {
			class = 1
		}
		points[i] = &nn.Point{
			Coords: []float64{digits[i].averageIntensity(), digits[i].horizontalSymmetry()},
			Class:  class,
		}
	}

	return points
}

func readDataFromFile(inputFile string) ([]*Digit, error) {
	var err error
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Failed to read file %s", inputFile)
	}

	digits := []*Digit{}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		digit, err := readNewDigit(strings.NewReader(scanner.Text()))
		if err != nil {
			return nil, err
		}

		digits = append(digits, digit)
	}

	return digits, nil
}
