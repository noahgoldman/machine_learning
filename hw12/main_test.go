package main

import (
	"testing"
)

func TestParseInput(t *testing.T) {
	data, err := readDataFromFile("data/ZipDigits.train")
	if err != nil {
		t.Fatal(err)
	}

	expected := 8998
	if len(data) != expected {
		t.Errorf("Read %d digits instead of %d", len(data), expected)
	}
}
