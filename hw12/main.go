package main

import (
	"flag"
	"fmt"
	"log"
	"math"

	"github.com/gonum/matrix/mat64"

	"./neural"
	"./nn"
	"./util"
)

const (
	MAX_K_NN    = 60
	CHOSEN_K_NN = 27
	FOLDS       = 10
	NN_GRID_DIM = 200

	MAX_K_RBF    = 60
	CHOSEN_K_RBF = 32

	P1_PERTURBATION = 0.0001
	NEURAL_M        = 10
	NEURAL_GRID_DIM = 1000
)

var (
	trainingFile string
	testingFile  string
	model        string
	printOutput  bool
	crossVal     bool

	p1a bool
)

func init() {
	flag.StringVar(&trainingFile, "i", "", "Training input file with data")
	flag.StringVar(&testingFile, "t", "", "Testing input file with data")
	flag.BoolVar(&printOutput, "p", false, "Set to pipe outputs to stdout")

	flag.StringVar(&model, "m", "", "The model to run")
	flag.BoolVar(&crossVal, "cv", false, "Run cross validation")
	flag.BoolVar(&p1a, "p1a", false, "Run Problem 1a")
}

func main() {
	flag.Parse()

	if p1a {
		problem_1a()
		return
	}

	if trainingFile == "" || testingFile == "" {
		log.Fatal("Must pass an input and testing file with -i and -t")
	}

	training_data, err := readDataFromFile(trainingFile)
	if err != nil {
		log.Fatal(err)
	}

	testing_data, err := readDataFromFile(testingFile)
	if err != nil {
		log.Fatal(err)
	}

	all_data := append(training_data, testing_data...)
	setNormalizationFromWeights(all_data)

	points := makeNNPoints(training_data)
	testing_pts := makeNNPoints(testing_data)

	if model == "nn" {
		if crossVal {
			knnCrossVal(points)
		} else {
			plotNN(points, testing_pts, training_data)
		}
	} else if model == "rbf" {
		if crossVal {
			rbfCrossVal(points)
		} else {
			rbf(points, testing_pts, training_data)
		}
	} else if model == "neural" {
		runNeural(training_data, testing_data)
	} else {
		log.Fatal("Enter a valid model to run")
	}
}

func runNeural(digits []*Digit, testing []*Digit) {
	xs, ys := constructMatrices(digits)
	xs_test, ys_test := constructMatrices(testing)
	ws := neural.MakeWeights(2, NEURAL_M)

	weights, eins := neural.VariableDescent(xs, ys, ws, neural.LINEAR, 0)
	plotNeuralBoundary(weights, neural.LINEAR, NEURAL_GRID_DIM, digits,
		"neural_boundary.png", "")
	plotEin(eins)
	fmt.Printf("Neural classifier created with Ein=%v, Etest=%v\n",
		neural.SampleError(xs, ys, weights, neural.LINEAR),
		neural.SampleError(xs_test, ys_test, weights, neural.LINEAR))

	decay_ws, _ := neural.VariableDescent(xs, ys, ws, neural.LINEAR, 0.01)
	plotNeuralBoundary(decay_ws, neural.LINEAR, NEURAL_GRID_DIM, digits,
		"neural_boundary_decay.png", "with weight decay")
	fmt.Printf("Neural (with decay) classifier created with Ein=%v, Etest=%v\n",
		neural.SampleError(xs, ys, decay_ws, neural.LINEAR),
		neural.SampleError(xs_test, ys_test, decay_ws, neural.LINEAR))

	early_stop := neural.EarlyStopping(xs, ys, ws, neural.LINEAR, 50)
	plotNeuralBoundary(early_stop, neural.LINEAR, NEURAL_GRID_DIM, digits,
		"neural_boundary_early.png", "with early stopping")
	fmt.Printf("Neural (early stop) classifier created with Ein=%v, Etest=%v\n",
		neural.SampleError(xs, ys, early_stop, neural.LINEAR),
		neural.SampleError(xs_test, ys_test, early_stop, neural.LINEAR))
}

func problem_1a() {
	m := 5

	xs := mat64.NewDense(1, 3, []float64{1, 1, 1})
	ys := mat64.NewVector(1, []float64{1})
	ws := []*mat64.Dense{
		neural.FillMatrix(3, m, 0.25),
		neural.FillMatrix(m+1, 1, 0.25),
	}

	ein, gradient := neural.ComputeEinGradient(xs, ys, ws, neural.LINEAR, 0)
	fmt.Printf("Linear: Got ein=%v and gradient:\n", ein)
	neural.PrintDenseArray(gradient)

	ein, gradient = neural.ComputeEinGradient(xs, ys, ws, neural.TANH, 0)
	fmt.Printf("Tanh: Got ein=%v and gradient:\n", ein)
	neural.PrintDenseArray(gradient)

	numerical_gradient := func(theta int) []*mat64.Dense {
		ngradient := make([]*mat64.Dense, len(ws))
		for i := range ws {
			r, c := ws[i].Dims()
			ngradient[i] = mat64.NewDense(r, c, nil)

			for j := 0; j < r; j++ {
				for k := 0; k < c; k++ {
					mod_ws := neural.CopyMatrices(ws)
					mod_ws[i].Set(j, k, ws[i].At(j, k)+P1_PERTURBATION)

					original := neural.Hypothesis(xs.RowView(0), ws, theta)
					modified := neural.Hypothesis(xs.RowView(0), mod_ws, theta)

					squaredError := func(x, y float64) float64 {
						return math.Pow(x-y, 2)
					}

					diff := (squaredError(modified, 1) - squaredError(original, 1)) / P1_PERTURBATION

					ngradient[i].Set(j, k, diff)
				}
			}
		}

		return ngradient
	}

	fmt.Print("=========\n")
	fmt.Print("NUMERICAL\n")
	fmt.Print("=========\n")

	fmt.Print("Linear:\n")
	neural.PrintDenseArray(numerical_gradient(neural.LINEAR))

	fmt.Print("Tanh:\n")
	neural.PrintDenseArray(numerical_gradient(neural.TANH))

}

func plotNN(points nn.Points, testing_pts nn.Points, digits []*Digit) {
	plotNNBoundaries(points, NN_GRID_DIM, CHOSEN_K_NN, digits)

	log.Printf("Cross validation error=%v",
		nn.CrossValidationFold(points, CHOSEN_K_NN, FOLDS))
	log.Printf("Sample error=%v", nn.SampleErrorK(points, points, CHOSEN_K_NN))
	log.Printf("Test error=%v", nn.SampleErrorK(points, testing_pts, CHOSEN_K_NN))
}

func knnCrossVal(points nn.Points) {
	ecv := make([]float64, 0, MAX_K_NN)
	labels := make([]float64, 0, MAX_K_NN)
	for k := 1; k <= MAX_K_NN; k += 2 {
		ecv = append(ecv, nn.CrossValidationFold(points, k, FOLDS))
		labels = append(labels, float64(k))
	}

	min_value, min_index := util.MinFloat(ecv)
	log.Printf("The minimum value is k=%v with E=%v", labels[min_index], min_value)

	plotEcv(ecv, labels)
}

func rbf(points nn.Points, testing nn.Points, digits []*Digit) {
	k := CHOSEN_K_RBF
	r := float64(2) / math.Sqrt(float64(k))
	weights, centers := RBF(points, k, r)

	// Make the transform function
	transform := func(x, y float64) []float64 {
		return rbfTransform(&nn.Point{Coords: []float64{x, y}}, centers, r)
	}

	plotWeights(weights, transform, digits)

	xs, ys := makeRBFMatrices(points, centers, k, r)
	xs_test, ys_test := makeRBFMatrices(testing, centers, k, r)

	// Make matrices for the testing points
	log.Printf("Sample error=%v", sampleError(xs, ys, weights))
	log.Printf("E_test=%v", sampleError(xs_test, ys_test, weights))
}

func rbfCrossVal(points nn.Points) {
	ecv := make([]float64, 0, MAX_K_RBF)
	labels := make([]float64, 0, MAX_K_RBF)
	for k := 1; k <= MAX_K_RBF; k++ {
		r := float64(2) / math.Sqrt(float64(k))
		centers := nn.MakeCenters(points, k)
		xs, ys := makeRBFMatrices(points, centers, k, r)
		ecv = append(ecv, crossValErrorLinear(xs, ys, 0))
		labels = append(labels, float64(k))
	}

	min_value, min_index := util.MinFloat(ecv)
	log.Printf("The minimum value is k=%v with E=%v", labels[min_index], min_value)

	plotEcv(ecv, labels)
}
