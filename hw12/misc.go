package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/gonum/matrix/mat64"
	"log"
	"os"
	"strings"

	"./nn"
)

func vectorToArray(vec *mat64.Vector) []float64 {
	rows, _ := vec.Dims()
	res := make([]float64, rows, rows)

	for i := 0; i < rows; i++ {
		res[i] = vec.At(i, 0)
	}

	return res
}

func printVectorJSON(vec *mat64.Vector) {
	if !printOutput {
		return
	}

	arr := vectorToArray(vec)
	res, err := json.Marshal(arr)
	if err != nil {
		log.Fatal("Failed to marshal: %s", err)
	}
	fmt.Fprintf(os.Stdout, "%s\n", res)
}

func identityMat(n int) *mat64.Dense {
	mat := mat64.NewDense(n, n, nil)
	for i := 0; i < n; i++ {
		mat.Set(i, i, 1)
	}
	return mat
}

func sign(x float64) float64 {
	if x > 0 {
		return 1
	} else if x < 0 {
		return -1
	}
	return 0
}

func constructMatrices(digits []*Digit) (*mat64.Dense, *mat64.Vector) {
	num := len(digits)

	// Create the matrices of x's and y's
	xs := make([]float64, num*3)
	ys := make([]float64, num)
	for i := range digits {
		if digits[i].digit == 1 {
			ys[i] = 1

		} else {
			ys[i] = -1
		}
		cur := 3 * i
		xs[cur] = 1
		xs[cur+1] = digits[i].averageIntensity()
		xs[cur+2] = digits[i].horizontalSymmetry()
	}

	return mat64.NewDense(num, 3, xs), mat64.NewVector(num, ys)
}

func makeNNPoints(digits []*Digit) []*nn.Point {
	points := make([]*nn.Point, len(digits))
	for i := range digits {
		class := -1
		if digits[i].digit == 1 {
			class = 1
		}
		points[i] = &nn.Point{
			Coords: []float64{digits[i].averageIntensity(), digits[i].horizontalSymmetry()},
			Class:  class,
		}
	}

	return points
}

func readDataFromFile(inputFile string) ([]*Digit, error) {
	var err error
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Failed to read file %s", inputFile)
	}

	digits := []*Digit{}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		digit, err := readNewDigit(strings.NewReader(scanner.Text()))
		if err != nil {
			return nil, err
		}

		digits = append(digits, digit)
	}

	return digits, nil
}
