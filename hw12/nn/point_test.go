package nn

import (
	"math"
	"sort"
	"testing"
)

func assertPointDistance(t *testing.T, p1 *Point, p2 *Point, expected float64) {
	actual := p1.distance(p2)
	if actual != expected {
		t.Errorf("Expected distance of %v not %v", expected, actual)
	}
}

func TestDistance(t *testing.T) {
	p1 := &Point{Coords: []float64{0, 0, 0}}
	p2 := &Point{Coords: []float64{1, 0, 0}}
	p3 := &Point{Coords: []float64{0, 1, 0}}

	assertPointDistance(t, p1, p2, 1)
	assertPointDistance(t, p1, p3, 1)
	assertPointDistance(t, p2, p3, math.Sqrt(2))
}

func TestSortPoints(t *testing.T) {
	p1 := &Point{Coords: []float64{0, 0, 0}, Class: 1}
	p2 := &Point{Coords: []float64{1, 0, 0}, Class: 2}
	p3 := &Point{Coords: []float64{0, 1, 0}, Class: 3}
	points := []*Point{p1, p2, p3}

	c := &Point{Coords: []float64{2, 0, 0}}

	sort.Sort(pointsByDistance{points: points, center: c})

	if points[0] != p2 || points[1] != p1 || points[2] != p3 {
		t.Errorf("Sort failed to get the correct order\tActual: %v", points)
	}
}

func TestRunKNN(t *testing.T) {
	p1 := &Point{Coords: []float64{0, 0, 0}, Class: 1}
	p2 := &Point{Coords: []float64{1, 0, 0}, Class: 2}
	p3 := &Point{Coords: []float64{0, 1, 0}, Class: 3}
	points := []*Point{p1, p2, p3}

	p := &Point{Coords: []float64{2, 0, 0}}

	if p.runKNN(points, 1) != 2 {
		t.Error("KNN failed")
	}
}
