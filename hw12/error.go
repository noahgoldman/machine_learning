package main

import (
	"github.com/gonum/matrix/mat64"
	"log"
	"math"

	"./util"
)

func sampleError(xs_mat mat64.Matrix, ys mat64.Matrix, weights *mat64.Vector) float64 {
	var errors float64 = 0
	xs := mat64.DenseCopyOf(xs_mat)

	rows, _ := xs.Dims()
	for i := 0; i < rows; i++ {
		vec := xs.RowView(i)

		if hypothesis(vec, weights) != ys.At(i, 0) {
			errors += 1
		}
	}

	return errors / float64(rows)
}

func hypothesis(x *mat64.Vector, w *mat64.Vector) float64 {
	product := new(mat64.Dense)
	product.Mul(w.T(), x)
	return util.Signf(product.At(0, 0))
}

func crossValErrorLinear(xs mat64.Matrix, ys mat64.Matrix, reg float64) float64 {
	hat := hatMatrix(xs, reg)
	yhat := yhatMatrix(hat, ys)

	var sum float64 = 0
	n, _ := ys.Dims()
	for i := 0; i < n; i++ {
		sum += math.Pow((yhat.At(i, 0)-ys.At(i, 0))/(1-hat.At(i, i)), 2)
	}

	return sum / float64(n)
}

func yhatMatrix(hat mat64.Matrix, ys mat64.Matrix) *mat64.Vector {
	yhat := new(mat64.Dense)
	yhat.Mul(hat, ys)

	ys_r, _ := ys.Dims()
	if r, c := yhat.Dims(); c > 1 || ys_r != r {
		log.Fatalf("Got yhat matrix with dimensions (%d x %d) instead of c=1", r, c)
	}

	return yhat.ColView(0)
}

func hatMatrix(xs mat64.Matrix, reg float64) *mat64.Dense {
	hat := new(mat64.Dense)
	hat.Mul(xs, regProduct(xs, reg))
	return hat
}
