package neural

import (
	"fmt"
	"math"
	"math/rand"

	"github.com/gonum/matrix/mat64"
)

const (
	VAR_ALPHA = 1.1
	VAR_BETA  = 0.8
	VAR_ETA0  = 0.01

	EARLY_STOP_ETA = 0.01
)

func ComputeEinGradient(xs *mat64.Dense, ys mat64.Matrix, ws []*mat64.Dense,
	theta int, decay float64) (float64, []*mat64.Dense) {

	ein := float64(0)
	gradient := make([]*mat64.Dense, len(ws))
	for i := range ws {
		r, c := ws[i].Dims()
		gradient[i] = FillMatrix(r, c, 0)
	}

	N, _ := xs.Dims()
	for i := 0; i < N; i++ {
		xsf, ss := forwardPropagation(xs.RowView(i), ws, theta)
		sens := backwardPropagation(xsf, ss, ws, ys.At(i, 0), theta)

		ein += (1 / float64(N)) * math.Pow(xsf[len(xsf)-1].At(0, 0)-ys.At(i, 0), 2)

		for j := range gradient {
			grad_xn := new(mat64.Dense)
			grad_xn.Mul(xsf[j], sens[j].T())

			grad_xn.Scale(1.0/float64(N), grad_xn)

			inter := new(mat64.Dense)
			inter.Add(gradient[j], grad_xn)

			gradient[j] = inter
		}
	}

	for i := range gradient {
		weight_decay := new(mat64.Dense)
		weight_decay.Scale(2.0*decay/float64(N), ws[i])

		gradient[i].Add(gradient[i], weight_decay)
	}

	return ein, gradient
}

func Hypothesis(x *mat64.Vector, ws []*mat64.Dense, theta int) float64 {
	xs, _ := forwardPropagation(x, ws, theta)

	// Make sure that the last element of xs is a scalar
	if r, c := xs[len(xs)-1].Dims(); r != 1 || c != 1 {
		panic(fmt.Errorf("xs^(L) is not a scalar but %dx%d", r, c))
	}

	return xs[len(xs)-1].At(0, 0)
}

func VariableDescent(xs *mat64.Dense, ys mat64.Matrix, ws_mat []*mat64.Dense,
	theta int, decay float64) ([]*mat64.Dense, []float64) {

	eins := make([]float64, 0)

	eta := VAR_ETA0
	ws := CopyMatrices(ws_mat)

	ein, gradient := ComputeEinGradient(xs, ys, ws, theta, decay)
	for i := 0; i < int(2*math.Pow10(3)); i++ {
		new_ws := make([]*mat64.Dense, len(ws))
		for i := range new_ws {
			scaled := new(mat64.Dense)
			scaled.Scale(eta, gradient[i])

			new_ws[i] = new(mat64.Dense)
			new_ws[i].Sub(ws[i], scaled)
		}

		new_ein, new_gradient := ComputeEinGradient(xs, ys, new_ws, theta, decay)
		if new_ein < ein {
			ws = new_ws
			eta *= VAR_ALPHA

			gradient = new_gradient
			ein = new_ein
		} else {
			eta *= VAR_BETA
		}

		eins = append(eins, ein)
	}

	return ws, eins
}

func EarlyStopping(xs *mat64.Dense, ys mat64.Matrix, ws_mat []*mat64.Dense,
	theta int, n int) []*mat64.Dense {

	xs, ys, xs_val, ys_val := chooseValidation(xs, ys, n)

	eta := VAR_ETA0
	ws := CopyMatrices(ws_mat)

	_, gradient := ComputeEinGradient(xs, ys, ws, theta, 0)
	ein_val, _ := ComputeEinGradient(xs_val, ys_val, ws, theta, 0)
	for {
		for i := range ws {
			scaled := new(mat64.Dense)
			scaled.Scale(eta, gradient[i])

			ws[i].Sub(ws[i], scaled)
		}

		_, gradient = ComputeEinGradient(xs, ys, ws, theta, 0)
		new_ein_val, _ := ComputeEinGradient(xs, ys, ws, theta, 0)

		if new_ein_val > ein_val {
			return ws
		} else {
			ein_val = new_ein_val
		}
	}

	panic("This should never be reached")
	return nil
}

func chooseValidation(xs *mat64.Dense, ys mat64.Matrix, n int) (*mat64.Dense,
	*mat64.Vector, *mat64.Dense, *mat64.Vector) {

	r, c := xs.Dims()
	xs_train_data := make([]float64, 0, (r-n)*c)
	ys_train_data := make([]float64, 0, (r-n)*c)
	xs_val_data := make([]float64, 0, n)
	ys_val_data := make([]float64, 0, n)

	validation_indices := rand.Perm(r)
	indices := make(map[int]bool)
	for i := range validation_indices {
		indices[validation_indices[i]] = i < 50
	}

	for i := 0; i < r; i++ {
		if indices[i] {
			xs_val_data = append(xs_val_data, xs.RawRowView(i)...)
			ys_val_data = append(ys_val_data, ys.At(i, 0))
		} else {
			xs_train_data = append(xs_train_data, xs.RawRowView(i)...)
			ys_train_data = append(ys_train_data, ys.At(i, 0))
		}
	}

	xs_train := mat64.NewDense(r-n, c, xs_train_data)
	ys_train := mat64.NewVector(r-n, ys_train_data)
	xs_val := mat64.NewDense(n, c, xs_val_data)
	ys_val := mat64.NewVector(n, ys_val_data)
	return xs_train, ys_train, xs_val, ys_val
}

func SampleError(xs *mat64.Dense, ys *mat64.Vector, ws []*mat64.Dense,
	theta int) float64 {

	N, _ := xs.Dims()
	error := 0.0
	for i := 0; i < N; i++ {
		if sign(Hypothesis(xs.RowView(i), ws, theta)) != ys.At(i, 0) {
			error += 1
		}
	}

	return error / float64(N)
}

func sign(x float64) float64 {
	if x > 0 {
		return 1
	} else if x < 0 {
		return -1
	}
	return 0
}

func CopyMatrices(mats []*mat64.Dense) []*mat64.Dense {
	res := make([]*mat64.Dense, len(mats))
	for i := range res {
		res[i] = mat64.DenseCopyOf(mats[i])
	}
	return res
}
