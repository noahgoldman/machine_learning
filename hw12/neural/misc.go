package neural

import (
	"fmt"
	"math/rand"

	"github.com/gonum/matrix/mat64"
)

const (
	WEIGHTS_STD_DEV = 0.1
)

func onesMatrix(r, c int) *mat64.Dense {
	return FillMatrix(r, c, 1)
}

func FillMatrix(r, c int, x float64) *mat64.Dense {
	vals := make([]float64, r*c)
	for i := range vals {
		vals[i] = x
	}

	return mat64.NewDense(r, c, vals)
}

func printDims(mat mat64.Matrix) {
	r, c := mat.Dims()
	fmt.Printf("The matrix %v has dimensions %dx%d\n", mat, r, c)
}

func PrintDenseArray(arr []*mat64.Dense) {
	fmt.Print("Matrices:\n")
	for i := range arr {
		fmt.Printf("\t%d: %#v\n", i, arr[i])
	}
	fmt.Print("\n")
}

func stripElemVec(vec *mat64.Vector, x int) *mat64.Vector {
	r, _ := vec.Dims()
	if x >= r {
		panic(fmt.Errorf("Can't remove element %d from the len=%d vector\n", x, r))
	}

	data := make([]float64, 0, r-1)
	for i := 0; i < r; i++ {
		if i != x {
			data = append(data, vec.At(i, 0))
		}
	}

	return mat64.NewVector(r-1, data)
}

// Make weights for a L=2 network with m hidden units and a single output node
func MakeWeights(d0, m int) []*mat64.Dense {
	randFunc := func() float64 {
		return rand.NormFloat64() * WEIGHTS_STD_DEV
	}

	w1_data := make([]float64, (d0+1)*m)
	for i := range w1_data {
		w1_data[i] = randFunc()
	}
	w1 := mat64.NewDense(d0+1, m, w1_data)

	w2_data := make([]float64, m+1)
	for i := range w2_data {
		w2_data[i] = randFunc()
	}
	w2 := mat64.NewDense(m+1, 1, w2_data)

	return []*mat64.Dense{w1, w2}
}
