package neural

import (
	"fmt"
	"math"

	"github.com/gonum/matrix/mat64"
)

const (
	TANH = iota
	LINEAR
)

func forwardPropagation(x0 *mat64.Vector, ws []*mat64.Dense,
	theta int) ([]*mat64.Vector, []*mat64.Vector) {

	L := len(ws)
	ss := make([]*mat64.Vector, L)
	xs := make([]*mat64.Vector, L+1)

	xs[0] = x0

	for i := 0; i < L; i++ {
		s_mat := new(mat64.Dense)
		s_mat.Mul(ws[i].T(), xs[i])
		ss[i] = s_mat.ColView(0)

		xs[i+1] = forwardTransform(ss[i], theta)
		if i != L-1 {
			xs[i+1] = addBiasColumn(xs[i+1])
		}
	}

	// Check that the final xs is a scalar
	if r, c := xs[len(xs)-1].Dims(); r != 1 || c != 1 {
		panic(fmt.Errorf("The final x value should be a scalar, not %dx%d", r, c))
	}

	return xs, ss
}

// Transform the vector by appending a one and applying a function to the rest
// of the elements
func forwardTransform(vec *mat64.Vector, theta int) *mat64.Vector {
	r, _ := vec.Dims()
	transformed := make([]float64, r)
	for i := 0; i < r; i++ {
		transformed[i] = thetaTransform(vec.At(i, 0), theta)
	}
	return mat64.NewVector(len(transformed), transformed)
}

func addBiasColumn(vec *mat64.Vector) *mat64.Vector {
	n, _ := vec.Dims()
	nvec := make([]float64, n+1)
	nvec[0] = 1
	for i := 1; i < len(nvec); i++ {
		nvec[i] = vec.At(i-1, 0)
	}

	return mat64.NewVector(len(nvec), nvec)
}

func thetaTransform(x float64, transform int) float64 {
	if transform == TANH {
		return math.Tanh(x)
	} else if transform == LINEAR {
		return x
	}

	panic(fmt.Errorf("Transform %d not known", transform))
	return 0
}
