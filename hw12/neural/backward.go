package neural

import (
	"fmt"
	"math"

	"github.com/gonum/matrix/mat64"
)

func backwardPropagation(xs []*mat64.Vector, ss []*mat64.Vector, ws []*mat64.Dense,
	y float64, theta int) []*mat64.Dense {

	L := len(ws)
	sens := make([]*mat64.Dense, L)
	sens[L-1] = initialSensitvity(xs[len(xs)-1].At(0, 0), y, theta)

	for i := L - 2; i >= 0; i-- {
		// Compute thetaprime(s^L)
		var tprime *mat64.Vector
		if theta == TANH {
			product := new(mat64.Dense)
			product.MulElem(xs[i+1], xs[i+1])

			tprime_mat := new(mat64.Dense)
			xs_r, xs_c := product.Dims()
			tprime_mat.Sub(onesMatrix(xs_r, xs_c), product)
			tprime = stripElemVec(tprime_mat.ColView(0), 0)
		} else if theta == LINEAR {
			xs_r, xs_c := xs[i+1].Dims()
			tprime = FillMatrix(xs_r-1, xs_c, 1).ColView(0)
		} else {
			panic(fmt.Errorf("theta should be LINEAR or TANH, not %d", theta))
		}

		p2 := new(mat64.Dense)
		p2.Mul(ws[i+1], sens[i+1])
		p2_stripped := stripElemVec(p2.ColView(0), 0)

		sens[i] = new(mat64.Dense)
		sens[i].MulElem(tprime, p2_stripped)
	}

	return sens
}

func initialSensitvity(xl float64, y float64, theta int) *mat64.Dense {
	thetaprime := float64(1)
	if theta == TANH {
		thetaprime = 1 - math.Pow(xl, 2)
	}

	res := 2 * (xl - y) * thetaprime
	return mat64.NewDense(1, 1, []float64{res})
}
