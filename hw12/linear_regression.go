package main

import (
	"github.com/gonum/matrix/mat64"
	"log"
)

func linRegWeights(xs mat64.Matrix, ys mat64.Matrix) *mat64.Vector {
	return linRegWeightsReg(xs, ys, 0)
}

func linRegWeightsReg(xs mat64.Matrix, ys mat64.Matrix, reg float64) *mat64.Vector {
	// (x^T*x + lambda*I)^-1 * x^t * y
	res := new(mat64.Dense)
	res.Mul(regProduct(xs, reg), ys)

	return res.ColView(0)
}

// Compute (x^T*x + I*lambda)^-1 * x^T
func regProduct(xs mat64.Matrix, reg float64) *mat64.Dense {
	// x^T * x
	x1 := new(mat64.Dense)
	x1.Mul(xs.T(), xs)

	// lambda*I
	regRes := new(mat64.Dense)
	_, cols := x1.Dims() // get the dims of the last result, should be square
	regRes.Scale(reg, identityMat(cols))

	// (x^T*x + lambda*I)^-1
	sum := new(mat64.Dense)
	sum.Add(x1, regRes)

	// (x^T*x + lambda*I)^-1
	x1_inv := new(mat64.Dense)
	err := x1_inv.Inverse(sum)
	if err != nil {
		log.Fatalf("Failed to invert matrix: %s", err)
	}

	// (x^T*x + lambda*I)^-1 * x^t
	x2 := new(mat64.Dense)
	x2.Mul(x1_inv, xs.T())

	return x2
}
