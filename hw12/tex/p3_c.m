% plot and output hyperplanes for problem 3
x = -150:1:150;
x_plane = 0*x;

zx = -5:0.001:5;
z_plane = zx.^3;

x1s = [1 -1];
x2s = [0 0];

figure
scatter(x1s, x2s); hold on;
plot(x_plane, x, zx, z_plane);

title('Plot of decision boundaries with and without the Z-space transform in the X-space');
xlabel('x_1');
ylabel('x_2');
oolegend('Data points','Without Z-space transform', 'With z-space transform');