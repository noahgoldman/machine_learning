\documentclass{article}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\usepackage{amsmath}
\usepackage{float}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage[margin=1.5in]{geometry}

\graphicspath{{images/}}


\begin{document}

\title{Homework 12}
\author{Noah Goldman}
\maketitle

\section{Neural Networks and Backpropagation}

\subsection{}

The backpropagation algorithm was implemented, and run on a 2 input, 2-hidden unit, 1 output sigmoidal neural network.  The gradient of the error surface was calculated for the data set with a single point $x_1 = \begin{bmatrix} 1 \\ 1 \end{bmatrix};y=1$.  This was performed for the identity output node transformation function, and the resulting gradient is shown below.

\newcommand{\nnLinear}{-0.1875}

\begin{align*}
  G_{linear}^{(1)} &= \begin{bmatrix}
    \nnLinear & \nnLinear \\
    \nnLinear & \nnLinear \\
    \nnLinear & \nnLinear
  \end{bmatrix} \\
  G_{linear}^{(2)} &= \begin{bmatrix}
    -0.75 \\ -0.5625 \\ -0.5625
  \end{bmatrix}
\end{align*}

The gradient of the error surface was also computed for the $tanh()$ output transformation function, and the result is given below.

\newcommand{\nnTanh}{-0.106826}

\begin{align*}
  G_{tanh}^{(1)} &= \begin{bmatrix}
    \nnTanh & \nnTanh \\
    \nnTanh & \nnTanh \\
    \nnTanh & \nnTanh
  \end{bmatrix} \\
  G_{tanh}^{(2)} &= \begin{bmatrix}
    -0.71625 \\ -0.454925 \\ -0.454925
  \end{bmatrix}
\end{align*}

\subsection{}

The gradient was determined numerically by perturbing a single weight individually by 0.0001, and then taking the difference between the output and the normal output divided by the perturbation to determine the gradient value.  This was repeated for each element in each weight vector.  The results for the identity output transformation are shown below.

\newcommand{\nnLinearNum}{-0.1874937}

\begin{align*}
  G_{linear_{num}}^{(1)} &= \begin{bmatrix}
    \nnLinearNum & \nnLinearNum \\
    \nnLinearNum & \nnLinearNum \\
    \nnLinearNum & \nnLinearNum
  \end{bmatrix} \\
  G_{linear_{num}}^{(2)} &= \begin{bmatrix}
    -0.7499 \\ -0.56244 \\ -0.56244
  \end{bmatrix}
\end{align*}

The gradient was also computed numerically for the $tanh()$ output transformation function, and the result is given below.

\newcommand{\nnTanhNum}{{-0.106817}}

\begin{align*}
  G_{tanh_{num}}^{(1)} &= \begin{bmatrix}
    \nnTanhNum & \nnTanhNum \\
    \nnTanhNum & \nnTanhNum \\
    \nnTanhNum & \nnTanhNum
  \end{bmatrix} \\
  G_{tanh_{num}}^{(2)} &= \begin{bmatrix}
    -0.71615 \\ -0.45489 \\ -0.45489
  \end{bmatrix}
\end{align*}

\section{Neural Network for Digits}

\subsection{}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p2_ein}
  \caption{$E_{in}(w)$ during variable learning rate gradient descent for digits data.}
  \label{p2_ein}
\end{figure}

The variable learning rate gradient descent heuristic was implemented for a neural network, and was used to generate a classifier for separating the digits '1' from 'not 1'.  Variable learning rate gradient descent was run for $2 \times 10^6$ iterations using the linear output transformation function, and a plot of $E_{in}(w)$ versus iterations is given above in Figure \ref{p2_ein}.  Since $E_{in}(w)$ showed effectively now change after $2 \times 10^3$, these values were not included in Figure \ref{p2_ein}.

Finally, the resulting classifier itself (along with the training data) is plotted in Figure \ref{p2_a}.  This classifier produced an in-sample error of $E_{in}(w)=0.076666$, and a test set error of $E_{test}(w)=0.09379862$.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p2_a}
  \caption{Neural network classifier for digits data.}
  \label{p2_a}
\end{figure}

\subsection{}

The backpropagation algorithm was modified to allow the use of weight decay.  This modified algorithm was then used with weight decay $\lambda=0.01/N$ to minimize the augmented error and thus generate a classifier for the digits data.  The classifier is shown in Figure \ref{p2_b}, and it resulted in an in-sample error of $E_{in}(w)=0.076666$ and a test set error of $E_{test}(w)=0.09390976$.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p2_b}
  \caption{Neural network classifier for digits data with weight decay.}
  \label{p2_b}
\end{figure}

\subsection{}

Early stopping was implemented for gradient descent, and the validation set size was set to 50 (with a training set size of 250).  The classifier with the minimum validation set error was computed, and is shown in Figure \ref{p2_c}.  This classifier had an in-sample error of $E_{in}(w)=0.06$ and a test set error of $E_{test}(w)=0.08079573$.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p2_c}
  \caption{Neural network classifier for digits data with early stopping.}
  \label{p2_c}
\end{figure}

\section{Support Vector Machines}

\subsection{}

As given in the text, the optimal hyperplane $(b^*, w^*)$ can be found through the constraints:

\begin{equation*}
  \begin{aligned}
    & \underset{b,w}{\text{minimize}}
    & & \frac{1}{2}w^T w \\
    & \text{subject to}
    & & \underset{n=1,\ldots,N}{\text{min}} y_n(w^T x_n + b) = 1
  \end{aligned}
\end{equation*}

Using the data points $x_1=(1,0),y_1=1$ and $x_2=(-1,0),y_2=-1$, the second element in the constraint can be expanded to form the two equations below.

\begin{align}
  w_1 + b &\geq 1 \\
  -(-w_1+b) & \geq 1
\end{align}

Adding together equations (1) and (2) above produces the simplified relationship $w_1 \geq 1$. Given this and the previously stated constraint in (1) that $w_1+b \geq 1$, it is clear that $b \geq 0$. The first optimal hyperplane constraint can then be simplified to $\frac{1}{2} w^T w=\frac{1}{2}(w_1^2+w_2^2)$.  This quantity is clearly minimized when $w_1=1$ and $w_2=0$ (as there is no constraint on $w_2$).  Therefore, we can say that the optimal hyperplane is $(b^*=0,w_1^*=1,w_2^*=0)$.

This optimal hyperplane and set of weights can be translated to an equation by solving $0=w^T x + b$ for the second dimension $x^2$, and substituting the optimal hyperplane weights.

\begin{align*}
  0 &= w^T x + b \\
  0 &= w_1^* x_1 + w_2^* x_2 + b^* \\
  0 &= x_1
\end{align*}

As shown above, the equation of the optimal hyperplane is $x_1=0$.
\\

In the $(x_1,x_2)$ space, the line segment connecting $x_1$ and $x_2$ is the line $x_2=0$ for $-1 \leq x_2 \leq 1$.  A line perpendicular to that would be $x_1=c$ for any $c \in [-1,1]$, so the optimal hyperplane must be perpendicular to the line segment connecting the points.  Since the midpoint of the line segment connecting $x_1$ and $x_2$ is at $x_1=0$, is it then clear that the hyperplane must be a perpendicular bisector of the line joining $x_1$ and $x_2$.

\subsection{}

Using the given transform $z=\begin{bmatrix}z_1 \\ z_2\end{bmatrix}=\begin{bmatrix}x_1^3-x_2 \\ x_1 x_2 \end{bmatrix}$, the data point $x_1$ becomes $z(x_1)=(1,0)$ and $x_2$ becomes $z(x_2)=(-1,0)$.
\\

As these data points in the z-space have the same coordinates as in the x-space, the optimal hyperplane for $z(x_1)$ and $z(x_2)$ will have the same weights $(b^*=0,w_1^*=1,w_2^*=0)$.  Again, this corresponds to the equation (in the z-space) $z_1=0$.

\subsection{}

In order to plot the decision boundary created using the data in the X-space, the equation $x_1=0$ can simply be used.  To plot the decision boundary constructed from transforming to the Z-space, but shown in the X-space, the hyperplane equation $z_1=0$ would become $x_1^3-x_2=0$.  Solving for $x_2$ produces $x_2=x_1^3$, which can be easily plotted in the X-space.  The plot of both decision boundaries is shown below in Figure \ref{p3_boundary}, where the -1 region is to the left of each boundary and the +1 region is to the right of each.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p3_c}
  \caption{Decision boundaries constructed using data in the X-space, and data transformed to the Z-space}.
  \label{p3_boundary}
\end{figure}

\subsection{}

For any vector $x=\begin{bmatrix} x_1 \\ x_2 \end{bmatrix}$, transforming into the Z-space produces $z(x)=\begin{bmatrix} x_1^3-x_2 \\ x_1 x_2 \end{bmatrix}$.  This can then be used to simplify the kernel function $K(x,y)=z(x) \cdot z(y)$ in terms of the components of $x$ and $y$.

\begin{align*}
  K(x,y) &= z(x) \cdot z(y) \\
  &= z(\begin{bmatrix} x_1 \\ x_2 \end{bmatrix}) \cdot z(\begin{bmatrix} y_1 \\ y_2 \end{bmatrix}) \\
  &= \begin{bmatrix} x_1^3 - x_2 \\ x_1 x_2 \end{bmatrix} \cdot \begin{bmatrix} y_1^3 - y_2 \\ y_1 y_2 \end{bmatrix} \\
  &= (x_1^3 - x_2)(y_1^3 - y_2) + (x_1 x_2)(y_1 y_2) \\
  &= x_1^3 y_1^3 - x_1^3 y_2 - x_2 y_1^3 + x_2 y_2 + x_1 x_2 y_1 y_2 \\
  &= x_1^3 y_1^3 - x_1^3 y_2 - x_2 y_1^3 + x_2 y_2 (1 + x_1 y_1) \\
\end{align*}

As shown above, the expression for the kernel function in terms of the components of $x$ and $y$ is $K(x,y)= x_1^3 y_1^3 - x_1^3 y_2 - x_2 y_1^3 + x_2 y_2 (1 + x_1 y_1)$.

\subsection{}

For a kernel function $K(x,y)$ with optimal hyperplane $(b^*=0, w_1^*=1, w_2^*=0)$, the classifier in the X-space should be $g(x)=sign(K(w*, x) + b^*)$.  This can be simplified to produce the functional form of the classifier.

\begin{align*}
  g(x) &= sign(K(w^*, x) + b^*) \\
  &= sign(K(\begin{bmatrix} w_1^* \\ w_2^* \end{bmatrix}, x) + b^* \\
  &= sign(K(\begin{bmatrix} 1 \\ 0 \end{bmatrix}, x) + 0) \\
  &= sign(1^3 x_1^3 - 1^3 x_2 - 0 x_1^3 + 0 x_2 (1 + y_1)
  &= sign(x_1^3 - x_2)
\end{align*}

Therefore, we can say that $g(x)=sign(x_1^3 - x_2)$.

\section{SVM with digits data}

\subsection{}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p4_a_small}
  \caption{SVM decision boundary for $C=0.01$}.
  \label{p4a_small}
\end{figure}

SVM was implemented and ran on the digits data.  A small value of the regularization parameter $C=0.01$ was first chosen, and a decision boundary was generated and is shown in Figure \ref{p4a_small}.  This resulted in a test set error of $E_{test}=0.046667$, and clearly exhibits significant underfitting.
\\

A larger value of the regularization parameter $C=10^8$ was then used to generate the the decision boundary shown in Figure \ref{p4a_large}.  This model had a test set error of $E_{test}=0.003333$, but shows very obivous overfitting due to the small regions it generates in the plot.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p4_a}
  \caption{SVM decision boundary for $C=10^8$}.
  \label{p4a_large}
\end{figure}

\subsection{}

As shown in part A, overfitting clearly increases as the value of $C$ increases.  Since an overfit hypothesis is more complex than an undefit hypothesis, we can say that in general, the complexity of decision boundaries generated with SVM increase as the regularization parameter $C$ increases.

\subsection{}

Cross-validation was performed on the training data set, and the resulting ideal value of the regularization parameter was chosen as $C=20$.  The resulting test error was $E_{test}=0.01666$, and the decision boundary is shown in Figure \ref{p4_c}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{p4_c}
  \caption{SVM decision boundary for $C=20$}.
  \label{p4_c}
\end{figure}

\section{Compare Methods: Linear, k-NN, RBF-network, Neural Network, SVM}

\begin{table}[H]
  \centering
  \caption{Test set errors for the models to be compared.}
  \label{compare}
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Model} & \textbf{$E_{test}$} \\ \hline
    Linear ($\lambda=1.23$) & 0.031118 \\
    27-NN rule & 0.031674 \\
    RBF-network ($k=32$ centers) & 0.031563 \\
    Neural network with early stopping & 0.080796 \\
    SVM with 8th order polynomial kernel and $C=20$ & 0.016666 \\ \hline
  \end{tabular}
\end{table}

Table \ref{compare} above shows the test errors coresponding to each of the different models being considered.  It is clear that the SVM with the 8th order polynomial kernel produces the best classifier, having a test error of effectively half that of the next lowest model.  However, SVM had by far the longest running time of all of the models, and performing cross-validation to determine the optimal value of the regularization parameter took a very long amount of time.  While SVM might be the most ideal model, in practice it might be very computationally intensive for a larger dataset.

The test set results also show that the linear, nearest neighbor, and RBF-network models all produce extremelly similar values of $E_{test}$.  Interestingly enough, the linear model (which is likely the simlest and fastest) appears to be just as effective as the more computationally challenging nearest neighbor and RBF-network models.  This may show that a simpler model may be just as good at approximating $E_{out}$ as a more compliated one.

Finally, the neural network model seems to be much worse than the others, as it has a test set error more than double of the next highest model.  There doesn't seem to be a clear reason why the neural network wasn't as effective, although it may be due to the fact that the training data set used was very well situated to use a linear classifier.  There was very clearly an area where a linear decision boundary could separate the data, and it's possible that the neural network model put too much weight on that.  Maybe with a different training data set, the neural network would have performed better on the test set.

\end{document}
