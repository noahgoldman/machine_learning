package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"strconv"
)

const (
	ImageDim = 16
)

var (
	intensityScale float64 = 1
	intensityShift float64 = 0

	symmetryScale float64 = 1
	symmetryShift float64 = 0
)

type digitImage [ImageDim][ImageDim]float64

type Digit struct {
	digit  int
	values digitImage

	normalizedIntensity float64
	normalizedSymmetry  float64
}

func readNewDigit(in io.Reader) (*Digit, error) {
	scanner := bufio.NewScanner(in)
	scanner.Split(bufio.ScanWords)

	// Read the initial digit
	if !scanner.Scan() {
		if scanner.Err() != nil {
			return nil, fmt.Errorf("Failed to read inital digit: %v",
				scanner.Err())
		}
	}
	digitText := scanner.Text()
	digitFloat, err := strconv.ParseFloat(digitText, 64)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse %s as an int", digitText)
	}
	digit := int(digitFloat)

	// Read in the actual data for the digit
	var image digitImage
	for i := 0; i < ImageDim; i++ {
		for j := 0; j < ImageDim; j++ {
			if !scanner.Scan() {
				fmt.Printf("got %v with digit: %#v\n", digit, image)
				return nil, fmt.Errorf("Failed to read all input digits: %v",
					scanner.Err())
			}
			val, err := strconv.ParseFloat(scanner.Text(), 64)
			if err != nil {
				return nil, fmt.Errorf("Failed to parse pixel value: %s", scanner.Text())
			}

			image[i][j] = val
		}
	}

	return &Digit{digit: digit, values: image}, nil
}

// Compute average intensity of the image
// Defined as summing all pixels, then dividing by ImageDim^2
func (d *Digit) averageIntensity() float64 {
	var sum float64 = 0.0
	for i := range d.values {
		for j := range d.values[i] {
			sum += d.values[i][j]
		}
	}

	return scaleShiftValue(sum/math.Pow(ImageDim, 2), intensityScale, intensityShift)
}

// Compute symmetry of the image around the vertical center line
// sum(2-abs((i,j) - (16-i, j)))
func (d *Digit) horizontalSymmetry() float64 {
	var sum float64 = 0.0
	for i := range d.values {
		for j := 0; j < len(d.values[i])/2; j++ {
			sum += 2 - math.Abs(d.values[i][j]-d.values[i][len(d.values[i])-1-j])
		}
	}

	return scaleShiftValue(sum/math.Pow(ImageDim, 2), symmetryScale, symmetryShift)
}

func scaleShiftValue(in float64, scale float64, shift float64) float64 {
	return in*scale + shift
}

func setNormalizationFromWeights(digits []*Digit) {
	intensityMax := -1 * math.MaxFloat64
	intensityMin := math.MaxFloat64
	symmetryMax := -1 * math.MaxFloat64
	symmetryMin := math.MaxFloat64

	for i := range digits {
		intensity := digits[i].averageIntensity()
		intensityMax = math.Max(intensityMax, intensity)
		intensityMin = math.Min(intensityMin, intensity)

		symmetry := digits[i].horizontalSymmetry()
		symmetryMax = math.Max(symmetryMax, symmetry)
		symmetryMin = math.Min(symmetryMin, symmetry)
	}

	scaleShift := func(min float64, max float64) (float64, float64) {
		return 2 / (max - min), (-2*min)/(max-min) - 1
	}

	intensityScale, intensityShift = scaleShift(intensityMin, intensityMax)
	symmetryScale, symmetryShift = scaleShift(symmetryMin, symmetryMax)
}
